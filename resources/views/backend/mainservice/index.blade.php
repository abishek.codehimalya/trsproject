@extends('backend.layouts.app')
@push('title')
Services
@endpush
@section('content')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.2.0/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.13.1/css/dataTables.bootstrap5.min.css">


            <div class="row">
                <div class="col-xl-12">
                    <div class="card">
                        <div class="card-header align-items-center d-flex">
                            <h4 class="card-title mb-0 flex-grow-1 text-end"><a href="{{ route('mainservice.create') }}" class="btn btn-primary">Add Service</a> </h4>

                        </div><!-- end card header -->

                        <div class="card-body">
                            @include('backend.layouts.message')
                            <div class="live-preview">
                                <div class="table-responsive">
                                    <table class="table align-middle table-nowrap mb-0" id="example">
                                        <thead>
                                            <tr>
                                                <th scope="col">ID</th>
                                                <th scope="col">Title</th>
                                                <th scope="col">Description</th>
                                                <th scope="col">ImageIcon</th>
                                                <th scope="col">Image</th>
                                                <th scope="col">Action</th>
                                            </tr>
                                        </thead>
                                        {{-- @foreach ($mainservices as $mainservice) --}}
                                        <tbody>
                                            @foreach ($mainservices as $mainservice)
                                            <tr>
                                                <td>{{ $loop->index + 1 }}</td>
                                                <td>{{ $mainservice->title }}</td>
                                                <td>{!! Str::limit($mainservice->description,50) !!}</td>
                                                <td><img src ="/mainserviceimage/{{ $mainservice->image }}"></td>
                                                <td><img src="/mainserviceimage/{{ $mainservice->detail_image }}" width="100px"></td>
                                                <td>
                                                    <div class="hstack gap-3 flex-wrap">
                                                        <a href="{{ route('mainservice.show', $mainservice->id) }}" class="link-success fs-15"><i class="ri-eye-fill align-middle"
                                                            style="color: blue;"></i></a>
                                                        <a href="{{ route('mainservice.edit', $mainservice->id) }}" class="link-success fs-15"><i class="ri-edit-2-line"></i></a>
                                                        <button type="submit" class="btn btn-sm" onclick="handleDelete({{ $mainservice->id }})" data-bs-toggle="modal" data-bs-target="#exampleModal"
                                                            style="color: red;">
                                                            <i class="ri-delete-bin-line"></i></button>
                                                    </div>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
@endsection

@push('modal')
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
         <form method="POST" id="deleteGalleryForm" action="">
            @csrf
            @method('DELETE')
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Delete Services</h4>
                    {{-- <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button> --}}
                </div>
                <div class="modal-body">
                    <p class="text-center text-bold">Are you sure you want to delete?</p>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-primary" data-bs-dismiss="modal">No, Go Back</button>
                    <button type="submit" class="btn btn-danger"data-bs-dismiss="modal">Yes, Delete</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </form>
    </div>
  </div>
</div>
@endpush

@push('js')
<script>
    function handleDelete(id) {
        var form = document.getElementById('deleteGalleryForm');
        form.action = 'mainservice/' + id;
        $('#exampleModal').modal('show');
    }
    </script>

<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.13.1/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.13.1/js/dataTables.bootstrap5.min.js"></script>
<script>
    $(document).ready(function () {
$('#example').DataTable();
});
</script>
@endpush
