@extends('backend.layouts.app')
@section('content')
@push('title')
View Service   
@endpush
<div class="container justify-content-center">
<div class="row mx-2">
        <div class="col-md-6 col-lg-12">
            <div class="card">
                <div class="card-header">
                    <a href="{{route('mainservice.index')}}" class="btn btn-secondary float-end"> Back</a>
                    MainService Detail  
                </div>
                <div class="card-body">
                    <p>ID : {{ $mainservice->id }}</p>
                    <p>Title : {{ $mainservice->title }}</p>
                    <p>Description : {!! $mainservice->description !!}</p>
                    <p>Image :<img src="/mainserviceimage/{{ $mainservice->image }}" width="100px"></p>
                    <p>Detail Image :<img src="/mainserviceimage/{{ $mainservice->detail_image }}" width="100px"></p>
                </div>
            </div>
        </div>
</div>
</div>

@endsection