@extends('backend.layouts.app')
@push('css')
{{-- <link href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" rel="stylesheet"> --}}
<!-- include summernote css/js -->
<link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
@endpush
@section('content')
@push('title')
Update Service
@endpush

<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header align-items-center d-flex">
                <h4 class="card-title mb-0 flex-grow-1">Edit Service</h4>

            </div><!-- end card header -->
            <div class="card-body">
                <div class="live-preview">
                    <div class="row gy-4">
                        @include('backend.layouts.message')
                        <form action="{{ route('mainservice.update', $mainservice->id) }}" method="POST" enctype="multipart/form-data">
                            @method('patch')
                            @csrf
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="mb-3">
                                        <label for="username" class="form-label">Title</label>
                                        <input type="text" name="title" class="form-control" id="username" value="{{ $mainservice->title  }}">
                                    </div>
                                </div>
                                <!--end col-->
                                <div class="col-lg-6">
                                    <div class="mb-3">
                                        <label for="exampleFormControlTextarea5" class="form-label">Image Icon</label>
                                        <input class="form-control" type="file" name="image" id="image" accept="image/*" onchange="readURL1(this);">
                                        @if (empty($mainservice->image))
                                        <img src="" width="100px" id="ms">
                                    @else
                                    <img src="/mainserviceimage/{{ $mainservice->image }}" width="100px" id="ms">
                                    @endif

                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="mb-3">
                                        <label><strong>Description :</strong></label>
                                        <textarea id="summernote" class="ckeditor form-control" name="description">{{ $mainservice->description }}</textarea>
                                    </div>
                                </div>
                                <!--end col-->
                                <div class="col-lg-6">
                                    <div class="mb-3">
                                        <label for="detail_image" class="form-label">Detail Image</label>
                                        <input class="form-control" type="file" name="detail_image" id="detail_image" accept="image/*" onchange="readURL(this);">
                                        @if (empty($mainservice->detail_image))
                                        <img src="" width="100px" id="md">
                                    @else
                                    <img src="/mainserviceimage/{{ $mainservice->detail_image }}" width="100px" id="md">
                                    @endif

                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12 d-flex justify-content-end gap-2">
                                    <button type="submit" class="btn btn-success ">Update</button>
                                    <a href="{{ route('mainservice.index') }}" class="btn btn-secondary ">Back</a>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!--end row-->
                </div>
            </div>
        </div>
    </div>
    <!--end col-->
</div>

@endsection

@push('js')
<script>
function readURL1(input){
    if(input.files && input.files[0]);
    var reader = new FileReader();
    reader.onload = function (e){
        $("#ms").attr('src', e.target.result).width(100);
    }
    reader.readAsDataURL(input.files[0])
}
</script>
<script>
    function readURL(input){
        if(input.files && input.files[0]);
        var reader = new FileReader();
        reader.onload = function (e){
            $("#md").attr('src', e.target.result).width(100);
        }
        reader.readAsDataURL(input.files[0])
    }
</script>


@endpush

@push('js')

 <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
 <script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
 <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>
 <script>
     $(document).ready(function() {
     $('#summernote').summernote();
   });
 </script>
 @endpush
