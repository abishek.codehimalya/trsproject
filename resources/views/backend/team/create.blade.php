@extends('backend.layouts.app')
@section('content')
@push('title')
Add Team
@endpush
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header align-items-center d-flex">
                <h4 class="card-title mb-0 flex-grow-1">Create Team</h4>
            </div><!-- end card header -->
            <div class="card-body">
                <div class="live-preview">
                    <div class="row gy-4">
                        @include('backend.layouts.message')
                        <form action="{{ route('teams.store') }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="mb-3">
                                        <label for="name" class="form-label">Name</label>
                                        <input type="text" name="name" class="form-control" id="name" value="{{ old('name') }}">
                                    </div>
                                </div>
                                <!--end col-->
                                <div class="col-lg-6">
                                    <div class="mb-3">
                                        <label for="title" class="form-label">Title</label>
                                        <input type="text" name="title" class="form-control" id="title" value="{{ old('title') }}">
                                    </div>
                                </div>
                                <!--end col-->
                                <div class="col-lg-6">
                                    <div class="mb-3">
                                        <label for="exampleFormControlTextarea5" class="form-label">Image</label>
                                        <input class="form-control" type="file" name="image" id="image" accept="image/*" onchange="readURL(this);">
                                        @if (empty($team->image))
                                        <img src="" width="100px" id="team">
                                    @else
                                      <img src="{{asset('teamimage/'.$team->image)}}" width="100px" id="team">                                  
                                    @endif
                                    </div>
                                </div>
                                <!--end col-->
                            </div>
                            <div class="row">
                                <div class="col-lg-12 d-flex justify-content-end gap-2">
                                    <button type="submit" class="btn btn-success ">Create</button>
                                    <a href="{{ route('teams.index') }}" class="btn btn-secondary ">Back</a>
                                </div>
                            </div>
                        </form>
                    </div>   
                    <!--end row-->
                </div>
            </div>
        </div>
    </div>
    <!--end col-->
</div>

@endsection

@push('js')
<script>
function readURL(input){
    if(input.files && input.files[0]);
    var reader = new FileReader();
    reader.onload = function (e){
        $("#team").attr('src', e.target.result).width(100);
    }
    reader.readAsDataURL(input.files[0])
}
</script>
@endpush

