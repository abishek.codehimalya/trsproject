@extends('backend.layouts.app')
@section('content')
@push('title')
View Team   
@endpush
<div class="container justify-content-center">
<div class="row mx-2">
        <div class="col-md-6 col-lg-12">
            <div class="card">
                <div class="card-header">
                    <a href="{{route('teams.index')}}" class="btn btn-secondary float-end"> Back</a>
                    MainService Detail  
                </div>
                <div class="card-body">
                    <p>ID : {{ $team->id }}</p>
                    <p>Name : {{ $team->title }}</p>
                    <p>Title : {!! $team->description !!}</p>
                    <p>Image :<img src="/teamimage/{{ $team->image }}" width="100px"></p>
                </div>
            </div>
        </div>
</div>
</div>

@endsection