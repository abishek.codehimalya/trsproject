@extends('backend.layouts.app')
@section('content')

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.2.0/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.13.1/css/dataTables.bootstrap5.min.css">

            <div class="row">
                <div class="col-xl-12">
                    <div class="card">
                        <div class="card-header align-items-center d-flex">
                            {{-- <h4 class="card-title mb-0 flex-grow-1 text-end"><a href="{{ route('countups.create') }}" class="btn btn-primary">Add CountUp</a> </h4> --}}
                           
                        </div><!-- end card header -->

                        <div class="card-body">
                            @include('backend.layouts.message')
                            <div class="live-preview">
                                <div class="table-responsive">
                                    <table class="table align-middle table-nowrap mb-0" id="example">
                                        <thead>
                                            <tr>
                                                <th scope="col">ID</th>
                                                <th scope="col">Count One</th>
                                                <th scope="col">Count Two</th>
                                                <th scope="col">Count Three</th>
                                                <th scope="col">Count Four</th>
                                                <th scope="col">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($countups as $countup)
                                            <tr>
                                                <td>{{ $countup->id }}</td>
                                                <td>{{ $countup->count_one }}</td>
                                                <td>{{ $countup->count_two }}</td>
                                                <td>{{ $countup->count_three }}</td>
                                                <td>{{ $countup->count_four }}</td>
                                                <td>
                                                    <div class="hstack gap-3 flex-wrap">
                                                        <a href="{{ route('countups.edit', $countup->id) }}" class="link-success fs-15"><i class="ri-edit-2-line"></i></a>
                                                        <form method="post" action="{{route('countups.destroy',$countup->id)}}">
                                                            @method('delete')
                                                            @csrf
                                                            <button type="submit" class="btn btn-danger btn-sm"><i class="ri-delete-bin-line"></i></button>
                                                        </form>
                                                    </div>
                                                </td>
                                            </tr> 
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
@endsection

@push('js')
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script src="https://cdn.datatables.net/1.13.1/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.13.1/js/dataTables.bootstrap5.min.js"></script>
    <script>
        $(document).ready(function () {
    $('#example').DataTable();
});
</script>
    
@endpush