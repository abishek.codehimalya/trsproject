@extends('backend.layouts.app')
@section('content')

<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header align-items-center d-flex">
                <h4 class="card-title mb-0 flex-grow-1">Create CountUp</h4>
             
            </div><!-- end card header -->
            <div class="card-body">
                <div class="live-preview">
                    <div class="row gy-4">
                        @include('backend.layouts.message')
                        <form action="{{ route('countups.store') }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="mb-3">
                                        <label for="count_one" class="form-label">Count One</label>
                                        <input type="text" name="count_one" class="form-control" id="count_one" >
                                    </div>
                                </div>
                                <!--end col-->
                                <div class="col-lg-6">
                                    <div class="mb-3">
                                        <label for="count_two" class="form-label">Count Two</label>
                                        <input type="text" name="count_two" class="form-control" id="count_two" >
                                    </div>
                                </div>
                                <!--end col-->
                                <div class="col-lg-6">
                                    <div class="mb-3">
                                        <label for="count_three" class="form-label">Count Three</label>
                                        <input type="text" name="count_three" class="form-control" id="count_three" >
                                    </div>
                                </div>
                                <!--end col-->
                                <div class="col-lg-6">
                                    <div class="mb-3">
                                        <label for="count_four" class="form-label">Count Four</label>
                                        <input type="text" name="count_four" class="form-control" id="count_four" >
                                    </div>
                                </div>
                                <!--end col-->
                                
                            </div>
                            <div class="row">
                                <div class="col-lg-12 d-flex justify-content-end gap-2">
                                    <button type="submit" class="btn btn-success ">Create</button>
                                    <a href="{{ route('countups.index') }}" class="btn btn-secondary ">Back</a>
                                </div>
                            </div>
                        </form>
                    </div>   
                    <!--end row-->
                </div>
            </div>
        </div>
    </div>
    <!--end col-->
</div>

@endsection
 @section('js')
 <script src="//cdn.ckeditor.com/4.14.0/standard/ckeditor.js"></script>
 <script type="text/javascript">
     $(document).ready(function() {
        $('.ckeditor').ckeditor();
     });
 </script>
 @endsection