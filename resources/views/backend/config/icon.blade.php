
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header align-items-center d-flex">
                <h4 class="card-title mb-0 flex-grow-1">Social Media </h4>
                
            </div><!-- end card header -->
            <div class="card-body">
                <div class="live-preview">
                    <div class="row gy-4">
                            <div class="row g-2">
                                <div class="col-lg-12">
                                    <div>
                                        <label for="logo_image" class="form-label">LOGO*</label>
                                        <input type="file" name="logo_image" class="form-control" value="{{ getConfiguration('logo_image') }}"  type="image/*" accept="image/*" onchange="readURL1(this);">
                                        <img src="{{ asset(getConfiguration('logo_image')) }}" alt="logo_image" id="logo" width="100px">
                                        @error('logo_image')
                                            <div class="invalid-feedback">
                                                {{ $message }}
                                            </div>    
                                        @enderror
                                    </div>
                                </div>
                                <!--end col-->
                                <div class="col-lg-12">
                                    <div>
                                        <label for="fabicon_image" class="form-label">Fabicon*</label>
                                        <input type="file" name="fabicon_image" class="form-control" id="fabicon_image"value="{{ getConfiguration('fabicon_image') }}" type="image/*" accept="image/*" onchange="readURL2(this);">
                                        <img src="{{ asset(getConfiguration('fabicon_image')) }}" alt="logo" id="fab">
                                        @error('fabicon_image')
                                            <div class="invalid-feedback">
                                                {{ $message }}
                                            </div>    
                                        @enderror
                                    </div>
                                </div>
                                <!--end col-->
                            </div>
                            <!--end row-->
                            <div class="col-lg-12">
                                <div class="d-flex justify-content-end">
                                    <button type="submit" class="btn btn-success">Save</button>
                                </div>
                            </div>

                            <!--end col-->
                    </div>   
                    <!--end row-->
                </div>
            </div>
        </div>
    </div>
    <!--end col-->
</div>

@push('js')
<script>
    function readURL1(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $("#logo").attr('src', e.target.result).width(100)
            };
            reader.readAsDataURL(input.files[0]);
        }
    }
</script> 
<script>
    function readURL2(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $("#fab").attr('src', e.target.result).width(100)
            };
            reader.readAsDataURL(input.files[0]);
        }
    }
</script> 
@endpush

