@extends('backend.layouts.app')
@push('css')
{{-- <link href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" rel="stylesheet"> --}}
<!-- include summernote css/js -->
<link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
@endpush
@section('content')
@push('title')
Settings
@endpush
@include('backend.layouts.message')
<form action="{{route('pageset.update')}}" method="POST" enctype="multipart/form-data">
  {{csrf_field()}}
<ul class="nav nav-tabs mx-3" id="myTab" role="tablist">
    <li class="nav-item" role="presentation">
      <button class="nav-link active" id="contact-tab" data-bs-toggle="tab" data-bs-target="#contact" type="button" role="tab" aria-controls="contact" aria-selected="false">CEO</button>
    </li>
    <li class="nav-item" role="presentation">
        <button class="nav-link" id="icon-tab" data-bs-toggle="tab" data-bs-target="#icon" type="button" role="tab" aria-controls="icon" aria-selected="false">Icon</button>
    </li>
    <li class="nav-item" role="presentation">
      <button class="nav-link " id="chef-tab" data-bs-toggle="tab" data-bs-target="#chef" type="button" role="tab" aria-controls="chef" aria-selected="true">ContactInfo</button>
    </li>
    <li class="nav-item" role="presentation">
      <button class="nav-link" id="social-tab" data-bs-toggle="tab" data-bs-target="#social" type="button" role="tab" aria-controls="social" aria-selected="false">SocialMedia</button>
    </li>
  </ul>

  <div class="tab-content mx-3" id="myTabContent">
    <div class="tab-pane fade show active" id="contact" role="tabpanel" aria-labelledby="contact-tab">
        @include('backend.config.ceo')
    </div>
    <div class="tab-pane fade" id="icon" role="tabpanel" aria-labelledby="icon-tab">
        @include('backend.config.icon')
    </div>
    <div class="tab-pane" id="chef" role="tabpanel" aria-labelledby="cher-tab">
        @include('backend.config.contactinfo')
    </div>
    <div class="tab-pane" id="social" role="tabpanel" aria-labelledby="about-tab">
      @include('backend.config.social')
    </div>
  </div>
</form>
@endsection

@push('js')
{{-- <script src="//cdn.ckeditor.com/4.14.0/standard/ckeditor.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
       $('.ckeditor').ckeditor();
    });
</script> --}}

<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>
<script>
    $(document).ready(function() {
    $('#summernote').summernote();
  });
</script>
@endpush

