
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header align-items-center d-flex">
                <h4 class="card-title mb-0 flex-grow-1">Social Media </h4>
                
            </div><!-- end card header -->
            <div class="card-body">
                <div class="live-preview">
                    <div class="row gy-4">
                            <div class="row g-2">
                                <div class="col-lg-4">
                                    <div>
                                        <label for="address" class="form-label">Address*</label>
                                        <input type="text" name="address" class="form-control" id="address" value="{{ getConfiguration('address') }}">
                                    </div>
                                </div>
                                <!--end col-->
                                <div class="col-lg-4">
                                    <div>
                                        <label for="primary_phone" class="form-label">Primary Phone*</label>
                                        <input type="text" name="primary_phone" class="form-control" id="primary_phone"  value="{{ getConfiguration('primary_phone') }}">
                                    </div>
                                </div>
                                <!--end col-->
                                <div class="col-lg-4">
                                    <div>
                                        <label for="secondary_phone" class="form-label">Secondary Phone*</label>
                                        <input type="text" name="secondary_phone" class="form-control" id="secondary_phone" value="{{ getConfiguration('secondary_phone') }}">
                                    </div>
                                </div>
                                <!--end col-->
                                <div class="col-lg-4">
                                    <div>
                                        <label for="primary_mail" class="form-label">Primary Email*</label>
                                        <input type="text" name="primary_mail" class="form-control" id="primary_mail"  value="{{ getConfiguration('primary_mail') }}">
                                    </div>
                                </div>
                                <!--end col-->
                                <div class="col-lg-4">
                                    <div>
                                        <label for="secondary_mail" class="form-label">Secondary Email*</label>
                                        <input type="text" name="secondary_mail" class="form-control" id="secondary_mail"  value="{{ getConfiguration('secondary_mail') }}">
                                    </div>
                                </div>
                                <!--end col-->
                                <div class="col-lg-4">
                                    <div>
                                        <label for="website" class="form-label">Website*</label>
                                        <input type="text" name="website" class="form-control" id="website"  value="{{ getConfiguration('website') }}">
                                    </div>
                                </div>
                                <!--end col-->
                            </div>
                            <!--end row-->
                            <div class="col-lg-4">
                                <div class="">
                                    <button type="submit" class="btn btn-primary">Save</button>
                                </div>
                            </div>
                            <!--end col-->
                    </div>   
                    <!--end row-->
                </div>
            </div>
        </div>
    </div>
    <!--end col-->
</div>

