
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header align-items-center d-flex">
                <h4 class="card-title mb-0 flex-grow-1">CEO</h4>

            </div><!-- end card header -->
            <div class="card-body">
                <div class="live-preview">
                    <div class="row gy-4">
                            <div class="row g-2">
                                <div class="col-lg-6">
                                    <div>
                                        <label for="ceo_name" class="form-label">Name*</label>
                                        <input type="text" name="ceo_name" class="form-control" id="ceo_name" value="{{ getConfiguration('ceo_name') }}">
                                    </div>
                                </div>
                                <!--end col-->
                                <div class="col-lg-6">
                                    <div>
                                        <label><strong>Description :</strong></label>
                                        <textarea id="summernote" class="ckeditor form-control" name="description">{!! getConfiguration('ceo_desc') !!}</textarea>
                                    </div>
                                </div>
                                <!--end col-->
                                <div class="col-lg-6">
                                    <div>
                                        <label for="ceo_title" class="form-label">Title*</label>
                                        <input type="text" name="ceo_title" class="form-control" id="ceo_title"  value="{{ getConfiguration('ceo_title') }}">
                                    </div>
                                </div>
                                <!--end col-->
                                <div class="col-lg-6">
                                    <div>
                                        <label for="ceo_image" class="form-label">Image*</label>
                                        <input type="file" name="ceo_image" class="form-control" value="{{ getConfiguration('ceo_image') }}" type="image/*" accept="image/*" onchange="readURL(this);">
                                        <img src="{{ asset(getConfiguration('ceo_image')) }}" alt="logo" id="ceo_image" width="100px">
                                        @error('fabicon_image')
                                            <div class="invalid-feedback">
                                                {{ $message }}
                                            </div>
                                        @enderror
                                    </div>
                                </div>
                                <!--end col-->
                            </div>
                            <!--end row-->
                            <div class="col-lg-12">
                                <div class="d-flex justify-content-end">
                                    <button type="submit" class="btn btn-success">Save</button>
                                </div>
                            </div>
                    </div>
                    <!--end row-->
                </div>
            </div>
        </div>
    </div>
    <!--end col-->
</div>

@push('js')
<script>
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $("#ceo_image").attr('src', e.target.result).width(100)
            };
            reader.readAsDataURL(input.files[0]);
        }
    }
</script>

<script src="//cdn.ckeditor.com/4.14.0/standard/ckeditor.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
       $('.ckeditor').ckeditor();
    });
</script>
@endpush

