
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header align-items-center d-flex">
                <h4 class="card-title mb-0 flex-grow-1">Social Media </h4>
               
            </div><!-- end card header -->
            <div class="card-body">
                <div class="live-preview">
                    <div class="row gy-4">
                            <div class="row g-2">
                                <div class="col-lg-6">
                                    <div>
                                        <label for="facebook" class="form-label">Facebook*</label>
                                        <input type="text" name="facebook" class="form-control" id="facebook"  value="{{ getConfiguration('facebook') }}">
                                    </div>
                                </div>
                                <!--end col-->
                                <div class="col-lg-6">
                                    <div>
                                        <label for="twitter" class="form-label">Twitter*</label>
                                        <input type="text" name="twitter" class="form-control" id="twitter"  value="{{ getConfiguration('twitter') }}" >
                                    </div>
                                </div>
                                <!--end col-->
                                <div class="col-lg-6">
                                    <div>
                                        <label for="linkedin" class="form-label">LinkedIn*</label>
                                        <input type="text" name="linkedin" class="form-control" id="linkedin" value="{{ getConfiguration('linkedin') }}" >
                                    </div>
                                </div>
                                <!--end col-->
                                <div class="col-lg-6">
                                    <div>
                                        <label for="instagram" class="form-label">Instagram*</label>
                                        <input type="text" name="instagram" class="form-control" id="instagram" value="{{ getConfiguration('instagram') }}" >
                                    </div>
                                </div>
                                <!--end col-->
                            </div>
                            <!--end row-->
                            <div class="col-lg-4">
                                <div class="">
                                    <button type="submit" class="btn btn-primary">Save</button>
                                </div>
                            </div>

                            <!--end col-->
                    </div>   
                    <!--end row-->
                </div>
            </div>
        </div>
    </div>
    <!--end col-->
</div>

