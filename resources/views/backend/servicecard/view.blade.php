@extends('backend.layouts.app')
@section('content')
@push('title')
View ServiceCard   
@endpush
<div class="container justify-content-center">
<div class="row mx-2">
        <div class="col-md-6 col-lg-12">
            <div class="card">
                <div class="card-header">
                    <a href="{{route('service-card.index')}}" class="btn btn-secondary float-end"> Back</a>
                    MainService Detail  
                </div>
                <div class="card-body">
                    <p>ID : {{ $service->id }}</p>
                    <p>Title : {{ $service->title }}</p>
                    <p>Description : {!! $service->description !!}</p>
                    <p>Image :<img src="/servicecardimage/{{ $service->image }}" width="100px"></p>
                </div>
            </div>
        </div>
</div>
</div>

@endsection