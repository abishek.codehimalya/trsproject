 <!-- JAVASCRIPT -->
 <script src="{{ asset('assets/libs/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
 <script src="{{ asset('assets/libs/simplebar/simplebar.min.js') }}"></script>
 <script src="{{ asset('}assets/libs/node-waves/waves.min.js') }}"></script>
 <script src="{{ asset('assets/libs/feather-icons/feather.min.js') }}"></script>
 <script src="{{ asset('assets/js/pages/plugins/lord-icon-2.1.0.js') }}"></script>
 <script src="{{ asset('assets/js/plugins.js') }}"></script>

 <!-- apexcharts -->
 <script src="{{ asset('assets/libs/apexcharts/apexcharts.min.js') }}"></script>

 <!-- Vector map-->
 <script src="{{ asset('assets/libs/jsvectormap/js/jsvectormap.min.js') }}"></script>
 <script src="{{ asset('assets/libs/jsvectormap/maps/world-merc.js') }}"></script>

 <!--Swiper slider js-->
 <script src="{{ asset('assets/libs/swiper/swiper-bundle.min.js') }}"></script>

 <!-- Dashboard init -->
 <script src="{{ asset('assets/js/pages/dashboard-ecommerce.init.js') }}"></script>

 <!-- App js -->
 <script src="{{ asset('assets/js/app.js') }}"></script>
 <!-- particles js -->
 <script src="{{ asset('assets/libs/particles.js/particles.js') }}"></script>
 <!-- particles app js -->
 <script src="{{ asset('assets/js/pages/particles.app.js') }}"></script>
 <!-- password-addon init -->
 <script src="{{ asset('assets/js/pages/password-addon.init.js') }}"></script>
  <!-- Sweet Alerts js -->
  <script src="assets/libs/sweetalert2/sweetalert2.min.js"></script>

  <!-- Sweet alert init js-->
  <script src="assets/js/pages/sweetalerts.init.js"></script>

  <!-- prismjs plugin -->
  <script src="{{ asset('assets/libs/prismjs/prism.js') }}"></script>

  <!-- ckeditor -->
  <script src="{{ asset('assets/libs/ckeditor/ckeditor5-build-classic/build/ckeditor.js') }}"></script>

  <!-- quill js -->
  <script src="{{ asset('assets/libs/quill/quill.min.js') }}assets/libs/quill/quill.min.js"></script>

  <!-- init js -->
  <script src="{{ asset('assets/js/pages/form-editor.init.js') }}"></script>



<script src="{{ asset('assets/js/jquery.slim.min.js') }}"></script>


@stack('js')
