
@if ($errors->any())
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

@if (Session::has('error'))

<div class="alert alert-danger alert-dismissible fade show" role="alert" id="alert">
{{ Session::get('error') }}
<button type="button" class=" btn-close" data-dismiss="alert" aria-label="Close">
</button>
</div>
@endif

@if (Session::has('success'))

<div class="alert alert-success alert-dismissible fade show" role="alert" id="fade">
  {{ Session::get('success') }}
  <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
  </button>
</div>
</div>
@endif

@section('js')
<script>
setTimeout(function() {
    $('#alert').fadeOut('fast');
}, 1000);
</script>
<script>
    setTimeout(function() {
        $('#fade').fadeOut('fast');
    }, 3000);
    </script>
@endsection