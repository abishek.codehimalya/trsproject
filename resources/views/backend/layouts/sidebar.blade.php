<div class="app-menu navbar-menu">
    <!-- LOGO -->
    <div class="navbar-brand-box">
            <a href="{{ route('index') }}" class="logo logo-light">
                <span class="logo-sm">
                    <img src="{{ asset(getConfiguration('logo_image')) }}" alt="" height="22" width="30">
                </span>
                <span class="logo-lg">
                    <img src="{{ asset(getConfiguration('logo_image')) }}" alt="" height="50" width="70">
                </span>
            </a>
        <button type="button" class="btn btn-sm p-0 fs-20 header-item float-end btn-vertical-sm-hover" id="vertical-hover">
            <i class="ri-record-circle-line"></i>
        </button>
    </div>

    <div id="scrollbar">
        <div class="container-fluid">
            <ul class="navbar-nav d-flex justify-content-center" id="navbar-nav">

                <li class="menu-title"><span data-key="t-menu">Menu</span></li>

                    <a class="nav-link menu-link @if(str_contains(Request::fullUrl(),'dashboard')) active @endif" href="{{ route('index') }}">
                        Dashboard</span>
                    </a>


            </ul>
            <ul class="navbar-nav justify-content-center" id="navbar-nav">

                    <a href="{{ route('blogs.index') }}" class="nav-link menu-link @if(str_contains(Request::fullUrl(),'blogs')) active @endif" > Blogs </a>

            </ul>
            <ul class="navbar-nav justify-content-center" id="navbar-nav">

                    <a href="{{ route('contactus.index') }}" class="nav-link menu-link @if(str_contains(Request::fullUrl(),'contactus')) active @endif" > Contacts </a>

            </ul>
            <ul class="navbar-nav justify-content-center" id="navbar-nav">
                    <a href="{{ route('faqs.index') }}" class="nav-link menu-link @if(str_contains(Request::fullUrl(),'faqs')) active @endif" > FAQ </a>
                </li>
            </ul>
            <ul class="navbar-nav justify-content-center" id="navbar-nav">
                <li class="nav-item">
                    <a href="{{ route('getupdate') }}" class="nav-link menu-link @if(str_contains(Request::fullUrl(),'update/content')) active @endif" > Home Page </a>
                </li>
            </ul>
            <ul class="navbar-nav justify-content-center" id="navbar-nav">
                <li class="nav-item">
                    <a href="{{ route('popularworks.index') }}" class="nav-link menu-link @if(str_contains(Request::fullUrl(),'popularworks')) active @endif"> Works </a>
                </li>
            </ul>
            <ul class="navbar-nav justify-content-center" id="navbar-nav">
                <li class="nav-item">
                    <a href="{{ route('pageset') }}" class="nav-link menu-link @if(str_contains(Request::fullUrl(),'settings')) active @endif" > Settings </a>
                </li>
            </ul>
            <ul class="navbar-nav justify-content-center" id="navbar-nav">
                <li class="nav-item">
                    <a href="{{ route('mainservice.index') }}" class="nav-link menu-link @if(str_contains(Request::fullUrl(),'mainservice')) active @endif" > Services </a>
                </li>
            </ul>
            <ul class="navbar-nav justify-content-center" id="navbar-nav">
                <li class="nav-item">
                    <a href="{{ route('getSubs') }}" class="nav-link menu-link @if(str_contains(Request::fullUrl(),'subscriber')) active @endif" > Subscribers </a>
                </li>
            </ul>
            <ul class="navbar-nav justify-content-center" id="navbar-nav">
                <li class="nav-item">
                    <a href="{{ route('teams.index') }}" class="nav-link menu-link @if(str_contains(Request::fullUrl(),'teams')) active @endif" > TeamMember </a>
                </li>
            </ul>
           
        </div>
        <!-- Sidebar -->
    </div>
</div>
