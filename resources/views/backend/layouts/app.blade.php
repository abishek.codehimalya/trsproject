<!doctype html>
<html lang="en" data-layout="vertical" data-topbar="light" data-sidebar="dark" data-sidebar-size="lg">
    @include('sweetalert::alert')

@include("backend.layouts.head");

<body>
    <!-- Begin page -->
    <div id="layout-wrapper">
        <header id="page-topbar">
         @include("backend.layouts.navbar")
        </header>

        <!-- ========== App Menu ========== -->
       @include("backend.layouts.sidebar")
        <!-- Left Sidebar End -->
        <!-- ============================================================== -->
        <!-- Start right Content here -->
        <!-- ============================================================== -->
        <div class="main-content">

            <div class="page-content">
                <div class="container-fluid">

                    <!-- start page title -->
                    <div class="row">
                        <div class="col-12">
                            <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                                <h4 class="mb-sm-0">@stack('title')</h4>

                                <div class="page-title-right">
                                    <ol class="breadcrumb m-0">
                                        <li class="breadcrumb-item"><a href="javascript: void(0);">Dashboard</a></li>
                                        <li class="breadcrumb-item active">@stack('title')</li>
                                    </ol>
                                </div>

                            </div>
                        </div>
                    </div>
                    <!-- end page title -->
                    {{-- <div class="container"> --}}
                    <div class="row">
                        <div class="col">

                           @yield('content')

                        </div> <!-- end col -->
                    </div>
                    {{-- </div> --}}

                </div>
                <!-- container-fluid -->
            </div>
            <!-- End Page-content -->
        </div>
        <!-- end main content-->
        @stack('modal')
    </div>
    <!-- END layout-wrapper -->
    @include("backend.layouts.footer")
    @yield('js')

</body>

</html>
