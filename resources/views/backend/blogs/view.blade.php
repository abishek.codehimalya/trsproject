@extends('backend.layouts.app')
@section('content')
@push('title')
View Blog  
@endpush
<div class="container justify-content-center">
<div class="row mx-2">
        <div class="col-md-6 col-lg-12">
            <div class="card">
                <div class="card-header">
                    <a href="{{route('blogs.index')}}" class="btn btn-secondary float-end"> Back</a>
                    Blog Detail  
                </div>
                <div class="card-body">
                    <p>ID : {{ $blog->id }}</p>
                    <p>Name : {{ $blog->name }}</p>
                    <p>Title : {{ $blog->title }}</p>
                    <p>Author : {!! $blog->description !!}</p>
                    <p>Image :<img src="/blogimage/{{ $blog->image }}" width="100px"></p>
                    <p>ImageDetail : {{ $blog->author }}</p>
                    <p>Date : {{ \Carbon\Carbon::parse($blog->date)->format('j F, Y')}}</p>
                </div>
            </div>
        </div>
</div>
</div>

@endsection