@extends('backend.layouts.app')
@push('css')
{{-- <link href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" rel="stylesheet"> --}}
<!-- include summernote css/js -->
<link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
@endpush
@push('title')
Update Blog
@endpush
@section('content')

<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header align-items-center d-flex">
                <h4 class="card-title mb-0 flex-grow-1">Edit Blogs</h4>

            </div><!-- end card header -->
            <div class="card-body">
                <div class="live-preview">
                    <div class="row gy-4">
                       @include('backend.layouts.message')
                        @if(isset($blog))
                        <form action="{{ route('blogs.update', $blog->id ) }}" method="POST" enctype="multipart/form-data">
                            @method('patch')
                            @else
                            @endif
                            @csrf
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="mb-3">
                                        <label for="name" class="form-label">Name</label>
                                        <input type="text" name="name" class="form-control" id="name" value="{{ $blog->name }}">
                                    </div>
                                </div>
                                <!--end col-->
                                <div class="col-lg-6">
                                    <div class="mb-3">
                                        <label for="title" class="form-label">Title</label>
                                        <input type="text" name="title" class="form-control" id="title" value="{{ $blog->title }}" >
                                    </div>
                                </div>
                                <!--end col-->
                                <div class="col-lg-6">
                                    <div class="mb-3">
                                        <label for="author" class="form-label">Author</label>
                                        <input class="form-control" name="author" id="author" type="text" value="{{ $blog->author }}">
                                    </div>
                                </div>
                                <!--end col-->
                                <div class="col-lg-6">
                                    <div class="mb-3">
                                        <label for="date" class="form-label">Date</label>
                                        <input type="date" name="date" class="form-control" data-provider="flatpickr" id="date" value="{{ $blog->date }}">
                                    </div>
                                </div>
                                <!--end col-->
                                <div class="col-lg-12">
                                    <div class="mb-3">
                                        <label for="exampleFormControlTextarea5" class="form-label">Image</label>
                                        <input class="form-control" type="file" name="image" id="image" accept="image/*" onchange="readURL(this);">
                                    </div>
                                    @if (empty($blog->image))
                                        <img src="" width="100px" id="blg">
                                    @else
                                      <img src="{{asset('blogimage/'.$blog->image)}}" width="100px" id="blg">
                                    @endif
                                </div>
                                <!--end col-->
                                <div class="col-lg-12">
                                    <div class="mb-3">
                                    <label><strong>Description :</strong></label>
                                    <textarea id="summernote" class="ckeditor form-control" name="description">{{ $blog->description }}</textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12 d-flex justify-content-end gap-2">
                                    <button type="submit" class="btn btn-success ">Update</button>
                                    <a href="{{ route('blogs.index') }}" class="btn btn-secondary ">Back</a>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!--end row-->
                </div>
            </div>
        </div>
    </div>
    <!--end col-->
</div>
{{-- {!! Toastr::message() !!} --}}

@endsection

@push('js')
<script>
function readURL(input){
    if(input.files && input.files[0]);
    var reader = new FileReader();
    reader.onload = function (e){
        $("#blg").attr('src', e.target.result).width(100);
    }
    reader.readAsDataURL(input.files[0])
}
</script>
@endpush
@push('js')
{{-- <script src="//cdn.ckeditor.com/4.14.0/standard/ckeditor.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
       $('.ckeditor').ckeditor();
    });
</script> --}}

@push('js')
{{-- <script src="//cdn.ckeditor.com/4.14.0/standard/ckeditor.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
       $('.ckeditor').ckeditor();
    });
</script> --}}

<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>
<script>
    $(document).ready(function() {
    $('#summernote').summernote();
  });
</script>
@endpush
@endpush
