@extends('backend.layouts.app')
@section('content')
@push('title')
View FAQ
@endpush
<div class="container justify-content-center">
<div class="row mx-2">
        <div class="col-md-6 col-lg-12">
            <div class="card">
                <div class="card-header">
                    <a href="{{route('faqs.index')}}" class="btn btn-secondary float-end"> Back</a>
                    FAQ Detail
                </div>
                <div class="card-body">
                    <p>ID : {{ $faq->id }}</p>
                    <p>Question : {!! $faq->question !!}</p>
                    <p>Answer : {!! $faq->answer !!}</p>
                </div>
            </div>
        </div>
</div>
</div>

@endsection
