@extends('backend.layouts.app')
@section('content')
@push('css')
<link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">    
@endpush
@push('title')
Update FAQ
@endpush
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header align-items-center d-flex">
                <h4 class="card-title mb-0 flex-grow-1">Edit FAQ</h4>

            </div><!-- end card header -->
            <div class="card-body">
                <div class="live-preview">
                    <div class="row gy-4">
                        @include('backend.layouts.message')
                        @if(isset($faq))
                        <form action="{{ route('faqs.update', $faq->id ) }}" method="POST" enctype="multipart/form-data">
                            @method('patch')
                            @else
                            @endif
                            @csrf
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="mb-3">
                                        <label for="question" class="form-label">Question</label>
                                    <textarea class="ckeditor form-control" id="goodnote" name="question">{!! $faq->question !!}</textarea>

                                    </div>
                                </div>
                                <!--end col-->
                                <div class="col-lg-6">
                                    <div class="mb-3">
                                        <label for="answer" class="form-label">Answer</label>
                                    <textarea class="ckeditor form-control" id="badnote" name="answer">{!! $faq->answer !!}</textarea>

                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12 d-flex justify-content-end gap-2">
                                    <button type="submit" class="btn btn-success text-end">Update</button>
                                    <a href="{{ route('faqs.index') }}" class="btn btn-secondary text-end">Back</a>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!--end row-->
                </div>
            </div>
        </div>
    </div>
    <!--end col-->
</div>

@endsection

@push('js')


<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>
<script>
    $(document).ready(function() {
    $('#goodnote').summernote();
  });
</script>
<script>
    $(document).ready(function() {
    $('#badnote').summernote();
  });
</script>


@endpush
