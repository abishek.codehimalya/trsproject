@extends('backend.layouts.app')
@section('content')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.2.0/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.13.1/css/dataTables.bootstrap5.min.css">
@push('title')
 About
@endpush

            <div class="row">
                <div class="col-xl-12">
                    <div class="card">
                        <div class="card-header align-items-center d-flex">
                            {{-- <h4 class="card-title mb-0 flex-grow-1 text-end"><a href="{{ route('offers.create') }}" class="btn btn-primary">Add Offer</a> </h4> --}}
                           
                        </div><!-- end card header -->

                        <div class="card-body">
                            @include('backend.layouts.message')
                            <div class="live-preview">
                                <div class="table-responsive">
                                    <table class="table align-middle table-nowrap mb-0" id="example">
                                        <thead>
                                            <tr>
                                                <th scope="col">ID</th>
                                                <th scope="col">Titles</th>
                                                <th scope="col">Description</th>
                                                <th scope="col">Image</th>
                                                <th scope="col">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($offers as $offer)
                                            <tr>
                                                <td>{{ $loop->index + 1 }}</td>
                                                <td>{{ $offer->title }}</td>
                                                <td>{!! Str::limit($offer->description,50) !!}</td>
                                                <td><img src="/offerimage/{{ $offer->image }}" width="100px"></td>
                                                <td>
                                                    <div class="hstack gap-3 flex-wrap">
                                                        <a href="{{ route('offers.show', $offer->id) }}" class="link-success fs-15"><i class="ri-eye-fill align-middle"></i></a>
                                                        <a href="{{ route('offers.edit', $offer->id) }}" class="link-success fs-15"><i class="ri-edit-2-line"></i></a>
                                                        <form method="post" action="{{route('offers.destroy',$offer->id)}}">
                                                            @method('delete')
                                                            @csrf
                                                            <button type="submit" class="btn btn-danger btn-sm"><i class="ri-delete-bin-line"></i></button>
                                                        </form>
                                                    </div>
                                                </td>
                                            </tr> 
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
@endsection

@push('js')
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script src="https://cdn.datatables.net/1.13.1/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.13.1/js/dataTables.bootstrap5.min.js"></script>
    <script>
        $(document).ready(function () {
    $('#example').DataTable();
});
</script>
    
@endpush