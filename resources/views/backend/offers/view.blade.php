@extends('backend.layouts.app')
@section('content')
@push('title')
About View   
@endpush
<div class="container justify-content-center">
<div class="row mx-2">
        <div class="col-md-6 col-lg-12">
            <div class="card">
                <div class="card-header">
                    <a href="{{route('offers.index')}}" class="btn btn-secondary float-end"> Back</a>
                    MainService Detail  
                </div>
                <div class="card-body">
                    <p>ID : {{ $offer->id }}</p>
                    <p>Title : {{ $offer->title }}</p>
                    <p>Description : {!! $offer->description !!}</p>
                    <p>Image :<img src="/offerimage/{{ $offer->image }}" width="100px"></p>
                </div>
            </div>
        </div>
</div>
</div>

@endsection