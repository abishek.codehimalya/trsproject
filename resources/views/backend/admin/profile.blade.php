@extends('backend.layouts.app')
@section('content')
@push('title')
Profile    
@endpush
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header align-items-center d-flex">
                <h4 class="card-title mb-0 flex-grow-1">Update Profile</h4>
               
            </div><!-- end card header -->
            <div class="card-body">
                <div class="live-preview">
                    <div class="row gy-4">
                        @include('backend.layouts.message')
                        <form action="{{ route('updateprofile',$admin->id) }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="mb-3">
                                        <label for="username" class="form-label">User Name</label>
                                        <input type="text" name="name" class="form-control" id="username" value="{{ $admin->name }}">
                                    </div>
                                </div>
                                <!--end col-->
                                <div class="col-lg-6">
                                    <div class="mb-3">
                                        <label for="emailInput" class="form-label">Email Address</label>
                                        <input type="email" name="email" class="form-control" id="emailInput"value="{{ $admin->email }}">
                                    </div>
                                </div>
                                <!--end col-->
                                <div class="col-lg-6">
                                    <div class="mb-3">
                                        <label for="phone" class="form-label">Phone</label>
                                        <input type="text" name="phone" class="form-control" id="phone" value="{{ $admin->phone }}">
                                    </div>
                                </div>
                                <!--end col-->
                                <div class="col-lg-6">
                                    <div class="mb-3">
                                        <label for="position" class="form-label">Position</label>
                                        <input type="text" name="position" class="form-control" id="position"  value="{{ $admin->position }}">
                                    </div>
                                </div>
                                <!--end col-->
                                <div class="col-lg-6">
                                    <div class="mb-3">
                                        <label for="image" class="form-label">Image</label>
                                        <input class="form-control" type="file" name="image" id="image" accept="image/*" onchange="readURL(this);">
                                    </div>
                                    @if (empty($admin->image)){{--to show profile image--}}
                                        <img src="{{asset('assets/images')}}" width="100px" id="one">
                                    @else
                                      <img src="{{asset('uploads/admin/'.$admin->image)}}" width="100px" id="one">                                  
                                    @endif
                                </div>
                                <!--end col-->
                            </div>
                            <div class="row">
                                <div class="col-lg-12 d-flex justify-content-end">
                                    <button type="submit" class="btn btn-success">Update Profile</button>
                                </div>
                            </div>
                        </form>
                    </div>   
                    <!--end row-->
                </div>
            </div>
        </div>
    </div>
    <!--end col-->
</div>
    
@endsection

@section('js')
<script>
function readURL(input){
    if(input.files && input.files[0]);
    var reader = new FileReader();
    reader.onload = function (e){
        $("#one").attr('src', e.target.result).width(100);
    }
    reader.readAsDataURL(input.files[0])
}
</script>
@endsection