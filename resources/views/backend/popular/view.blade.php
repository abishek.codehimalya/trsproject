@extends('backend.layouts.app')
@section('content')
@push('title')
View PopularWork   
@endpush
<div class="container justify-content-center">
<div class="row mx-2">
        <div class="col-md-6 col-lg-12">
            <div class="card">
                <div class="card-header">
                    <a href="{{route('popularworks.index')}}" class="btn btn-secondary float-end"> Back</a>
                    MainService Detail  
                </div>
                <div class="card-body">
                    <p>ID : {{ $popular->id }}</p>
                    <p>Image Title : {{ $popular->image_title }}</p>
                    <p>Image :<img src="/popularimage/{{ $popular->image }}" width="100px"></p>
                </div>
            </div>
        </div>
</div>
</div>

@endsection