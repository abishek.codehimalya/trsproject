
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header align-items-center d-flex">
                <h4 class="card-title mb-0 flex-grow-1">Edit ServiceCard</h4>

            </div><!-- end card header -->
            <div class="card-body">
                <div class="live-preview">
                    <div class="row gy-4">
                        {{-- @include('backend.layouts.message') --}}
                        <form action="{{ route('service-card.update', $service->id) }}"method="POST" enctype="multipart/form-data">
                            @method('patch')
                            @csrf
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="mb-3">
                                        <label for="title1" class="form-label">Title One</label>
                                        <input type="text" name="title1" class="form-control" id="title1" value="{{ $service->title1  }}">
                                    </div>
                                </div>
                                <!--end col-->
                                <div class="col-lg-6">
                                    <div class="mb-3">
                                        <label for="exampleFormControlTextarea5" class="form-label">Image One</label>
                                        <input class="form-control" type="file" name="image1" id="image" accept="image/*" onchange="readURL1(this);">
                                        @if (empty($service->image1))
                                        <img src="" width="100px" id="first_image">
                                        @else
                                        <img src="/servicecardimage/{{ $service->image1 }}" width="100px" id="first_image">
                                        @endif
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="mb-3">
                                    <label><strong>Description One :</strong></label>
                                    <textarea id="summernote1" class="ckeditor form-control" name="desc1">{{ $service->desc1 }}</textarea>
                                    </div>
                                </div>
                                <!--end col-->
                                <div class="col-lg-6">
                                    <div class="mb-3">
                                        <label for="title2" class="form-label">Title Two</label>
                                        <input type="text" name="title2" class="form-control" id="title2" value="{{ $service->title2  }}">
                                    </div>
                                </div>
                                <!--end col-->
                                <div class="col-lg-6">
                                    <div class="mb-3">
                                        <label for="exampleFormControlTextarea5" class="form-label">Image Two</label>
                                        <input class="form-control" type="file" name="image2" id="image" accept="image/*" onchange="readURL2(this);">
                                        @if (empty($service->image2))
                                        <img src="" width="100px" id="second_image">
                                    @else
                                      <img src="/servicecardimage/{{ $service->image2 }}" width="100px" id="second_image">
                                    @endif
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="mb-3">
                                    <label><strong>Description Two :</strong></label>
                                    <textarea id="summernote2" class="ckeditor form-control" name="desc2">{{ $service->desc2 }}</textarea>
                                    </div>
                                </div>
                                <!--end col-->
                                <div class="col-lg-6">
                                    <div class="mb-3">
                                        <label for="title3" class="form-label">Title Three</label>
                                        <input type="text" name="title3" class="form-control" id="title3" value="{{ $service->title3  }}">
                                    </div>
                                </div>
                                <!--end col-->
                                <div class="col-lg-6">
                                    <div class="mb-3">
                                        <label for="exampleFormControlTextarea5" class="form-label">Image Three</label>
                                        <input class="form-control" type="file" name="image3" id="image" accept="image/*" onchange="readURL3(this);">
                                        @if (empty($service->image3))
                                        <img src="" width="100px" id="third_image">
                                    @else
                                      <img src="/servicecardimage/{{ $service->image3 }}" width="100px" id="third_image">
                                    @endif
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="mb-3">
                                    <label><strong>Description Three :</strong></label>
                                    <textarea id="summernote3" class="ckeditor form-control" name="desc3">{{ $service->desc3 }}</textarea>
                                    </div>
                                </div>
                                <!--end col-->
                            </div>
                            <div class="row">
                                <div class="col-lg-12 d-flex justify-content-end gap-2">
                                    <button type="submit" class="btn btn-success ">Update</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!--end row-->
                </div>
            </div>
        </div>
    </div>
    <!--end col-->
</div>


@push('js')
<script>
function readURL1(input){
    if(input.files && input.files[0]);
    var reader = new FileReader();
    reader.onload = function (e){
        $("#first_image").attr('src', e.target.result).width(100);
    }
    reader.readAsDataURL(input.files[0])
}
</script>
<script>
    function readURL2(input){
        if(input.files && input.files[0]);
        var reader = new FileReader();
        reader.onload = function (e){
            $("#second_image").attr('src', e.target.result).width(100);
        }
        reader.readAsDataURL(input.files[0])
    }
</script>
    <script>
        function readURL3(input){
            if(input.files && input.files[0]);
            var reader = new FileReader();
            reader.onload = function (e){
                $("#third_image").attr('src', e.target.result).width(100);
            }
            reader.readAsDataURL(input.files[0])
        }
    </script>


@endpush
