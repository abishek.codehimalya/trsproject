@extends('backend.layouts.app')
@push('css')
{{-- <link href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" rel="stylesheet"> --}}
<!-- include summernote css/js -->
<link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
@endpush
@push('title')
Update
@endpush
@section('content')

@include('backend.layouts.message')
{{-- <form action="#" method="POST" enctype="multipart/form-data">
  {{csrf_field()}} --}}
<ul class="nav nav-tabs mx-3" id="myTab" role="tablist">
    <li class="nav-item" role="presentation">
      <button class="nav-link active" id="contact-tab" data-bs-toggle="tab" data-bs-target="#contact" type="button" role="tab" aria-controls="contact" aria-selected="false">About</button>
    </li>
    <li class="nav-item" role="presentation">
        <button class="nav-link" id="icon-tab" data-bs-toggle="tab" data-bs-target="#icon" type="button" role="tab" aria-controls="icon" aria-selected="false">Count</button>
    </li>
    <li class="nav-item" role="presentation">
      <button class="nav-link " id="chef-tab" data-bs-toggle="tab" data-bs-target="#chef" type="button" role="tab" aria-controls="chef" aria-selected="true">ServiceCard</button>
    </li>
  </ul>

  <div class="tab-content mx-3" id="myTabContent">
    <div class="tab-pane fade show active" id="contact" role="tabpanel" aria-labelledby="contact-tab">
        @include('backend.updatetabs.about')
    </div>
    <div class="tab-pane fade" id="icon" role="tabpanel" aria-labelledby="icon-tab">
        @include('backend.updatetabs.count')
    </div>
    <div class="tab-pane" id="chef" role="tabpanel" aria-labelledby="che-tab">
        @include('backend.updatetabs.servicecard')
    </div>
  </div>
{{-- </form> --}}
@endsection


@push('js')
{{-- <script src="//cdn.ckeditor.com/4.14.0/standard/ckeditor.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
       $('.ckeditor').ckeditor();
    });
</script> --}}

<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>
<script>
    $(document).ready(function() {
    $('#summernote').summernote();
    $('#summernote1').summernote();
    $('#summernote2').summernote();
    $('#summernote3').summernote();
  });
</script>
@endpush
