
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header align-items-center d-flex">
                <h4 class="card-title mb-0 flex-grow-1">About</h4>

            </div><!-- end card header -->
            <div class="card-body">
                <div class="live-preview">
                    <div class="row gy-4">
                        {{-- @include('backend.layouts.message') --}}
                        <form action="{{ route('offers.update', $offer->id) }}" method="POST" enctype="multipart/form-data">
                            @method('patch')
                            @csrf
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="mb-3">
                                        <label for="username" class="form-label">Title</label>
                                        <input type="text" name="title" class="form-control" id="username" value="{{ $offer->title  }}">
                                    </div>
                                </div>
                                <!--end col-->
                                <div class="col-lg-6">
                                    <div class="mb-3">
                                        <label for="exampleFormControlTextarea5" class="form-label">Image</label>
                                        <input class="form-control" type="file" name="image" id="image" accept="image/*" onchange="readURL(this);">
                                        @if (empty($offer->image))
                                        <img src="" width="100px" id="offer">
                                    @else
                                    <img src="/offerimage/{{ $offer->image }}" width="100px" id="offer">
                                    @endif

                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="mb-3">
                                    <label><strong>Description :</strong></label>
                                    <textarea id="summernote" class="ckeditor form-control" name="description">{{ $offer->description }}</textarea>
                                    </div>
                                </div>
                                <!--end col-->
                            </div>
                            <div class="row">
                                <div class="col-lg-12 d-flex justify-content-end gap-2">
                                    <button type="submit" class="btn btn-success ">Update</button>
                                    {{-- <a href="{{ route('offers.index') }}" class="btn btn-secondary ">Back</a> --}}
                                </div>
                            </div>
                        </form>
                    </div>
                    <!--end row-->
                </div>
            </div>
        </div>
    </div>
    <!--end col-->
</div>


@push('js')
<script>
function readURL(input){
    if(input.files && input.files[0]);
    var reader = new FileReader();
    reader.onload = function (e){
        $("#offer").attr('src', e.target.result).width(100);
    }
    reader.readAsDataURL(input.files[0])
}
</script>

<script src="//cdn.ckeditor.com/4.14.0/standard/ckeditor.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
       $('.ckeditor').ckeditor();
    });
</script>
@endpush

