
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header align-items-center d-flex">
                <h4 class="card-title mb-0 flex-grow-1">Edit CountUp</h4>
               
            </div><!-- end card header -->
            <div class="card-body">
                <div class="live-preview">
                    <div class="row gy-4">
                        {{-- @include('backend.layouts.message') --}}
                        <form action="{{ route('countups.update', $countup->id) }}" method="POST" enctype="multipart/form-data">
                            @method('patch')
                            @csrf
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="mb-3">
                                        <label for="count_one" class="form-label">Posititve Feedback</label>
                                        <input type="text" name="count_one" class="form-control" id="count_one" value="{{ $countup->count_one }}">
                                    </div>
                                </div>
                                <!--end col-->
                                <div class="col-lg-6">
                                    <div class="mb-3">
                                        <label for="count_two" class="form-label">Positive Feedback</label>
                                        <input type="text" name="count_two" class="form-control" id="count_two"value="{{ $countup->count_two  }}" >
                                    </div>
                                </div>
                                <!--end col-->
                                <div class="col-lg-6">
                                    <div class="mb-3">
                                        <label for="count_three" class="form-label">Branch Office</label>
                                        <input type="text" name="count_three" class="form-control" id="count_three" value="{{ $countup->count_three }}">
                                    </div>
                                </div>
                                <!--end col-->
                                <div class="col-lg-6">
                                    <div class="mb-3">
                                        <label for="count_four" class="form-label">Expert Advisor</label>
                                        <input type="text" name="count_four" class="form-control" id="count_four" value="{{ $countup->count_four }}">
                                    </div>
                                </div>
                                <!--end col-->
                                
                            </div>
                            <div class="row">
                                <div class="col-lg-12 d-flex justify-content-end gap-2">
                                    <button type="submit" class="btn btn-success ">Update</button>
                                    {{-- <a href="{{ route('countups.index') }}" class="btn btn-secondary ">Back</a> --}}
                                </div>
                            </div>
                        </form>
                    </div>   
                    <!--end row-->
                </div>
            </div>
        </div>
    </div>
    <!--end col-->
</div>


@section('js')

<script src="//cdn.ckeditor.com/4.14.0/standard/ckeditor.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
       $('.ckeditor').ckeditor();
    });
</script>
@endsection