<!DOCTYPE html>
<html lang="en">
 @include('front.partials.styles')
  <body>
    <!--==================== Preloader Start ====================-->
    <div class="preloder" id="page-preloader">
      <div class="circle"></div>
    </div>
    <!--==================== Preloader End ====================-->

    <!-- ==================== Header Start Here ==================== -->
 @include('front.partials.header')
    <!-- ==================== Header End Here ==================== -->

    <!-- ==================== Banner Start Here ==================== -->
    <section class="breadcumb-section bg-overlay-one" style="background-image: url(/assets/frontend/images/breadcumb/breadcumb-img.png);">
        <div class="container">
          <div class="row">
            <div class="col-lg-12">
              <div class="breadcumb text-center">
                <h1 class="breadcumb-title text-white">LATEST NEWS</h1>
                <ul class="breadcumb-list d-flex justify-content-center">
                  <li><a href="{{ route('getHome') }}"> Home </a></li>
                  <li><span> - </span></li>
                  <li style="color: rgb(5, 54, 133)">Latest news</li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </section>
    <!-- ==================== Banner End Here ==================== -->

    <!-- ==================== Blogs Starts Here ==================== -->

    <section class="blog pt-120 pb-60">
        <div class="container">
          <div class="row gy-4 justify-content-center">
            @foreach ($frontblog as $blog)
            <div class="col-lg-4 col-md-6">
              <div class="blog-item">
                <div class="thumb">
                  <img src="{{ asset('blogimage') }}/{{ $blog->image }}" alt="" />
                </div>
                <div class="blog-content">
                  <span class="category">{{ $blog->name }}</span>
                  <h4 class="title">
                    <a href="{{ route('getBlogDetail', $blog->slug) }}">{{ $blog->title }}</a>
                  </h4>
                  <div class="blog-meta d-flex justify-content-between flex-wrap">
                    <ul class="meta d-flex flex-wrap justify-content-center">
                      <li>{{ $blog->author }}</li>
                      <li>|</li>
                      <li>{{ \Carbon\Carbon::parse($blog->date)->format('j F, Y') }}</li>
                    </ul>
                    <div class="blog-share">
                      <a href="javascript: void(0)"
                        ><i class="fas fa-share-alt"></i
                      ></a>
                      <div class="popup-share-icons">
                        <ul class="popup-icons d-flex flex-wrap">
                          <li>
                            <a href="#"><i class="fab fa-facebook-f"></i></a>
                          </li>
                          <li>
                            <a href="#"><i class="fab fa-twitter"></i></a>
                          </li>
                          <li>
                            <a href="#"><i class="fab fa-linkedin-in"></i></a>
                          </li>
                          <li>
                            <a href="#"><i class="fab fa-instagram"></i></a>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            @endforeach
            <!-- Pagination -->
            
          </div>
        </div>
      </section>

    <!-- ==================== Blogs End Here ==================== -->

    <!-- ==================== CTA Start Here ==================== -->
   @include('front.partials.cta')
    <!-- ==================== CTA End Here ==================== -->

    <!-- ==================== Footer End Here ==================== -->
   @include('front.partials.footer')

    <div class="two"></div>
    <!-- ==================== Footer End Here ==================== -->

    <!-- ==================== Scroll to Top End Here ==================== -->
    <a class="scroll-top"><i class="fas fa-angle-double-up"></i></a>
    <!-- ==================== Scroll to Top End Here ==================== -->

  @include('front.partials.scripts')
  </body>
</html>
