<!DOCTYPE html>
<html lang="en">
 @include('front.partials.styles')
  <body>
    <!--==================== Preloader Start ====================-->
    <div class="preloder" id="page-preloader">
      <div class="circle"></div>
    </div>
    <!--==================== Preloader End ====================-->

    <!-- ==================== Header Start Here ==================== -->
 @include('front.partials.header')
    <!-- ==================== Header End Here ==================== -->

    <!-- ==================== Banner Start Here ==================== -->
    <section
    class="breadcumb-section bg-overlay-one" style="background-image: url(/assets/frontend/images/breadcumb/breadcumb-img.png);">
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <div class="breadcumb text-center">
            <h1 class="breadcumb-title text-white">NEWS DETAILS</h1>
            <ul class="breadcumb-list d-flex justify-content-center">
              <li><a href="{{ route('getHome') }}"> Home </a></li>
              <li><span> - </span></li>
              <li><a href="{{ route('getBlog') }}">Latest news</a></li>
              <li><span> - </span></li>
              <li style="color: rgb(5, 54, 133)">News Details</li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </section>
    <!-- ==================== Banner End Here ==================== -->

    <!-- ==================== Blogs Starts Here ==================== -->

    <section class="blog-details pt-120 pb-60">
        <div class="container">
          <div class="row gy-lg-5 gy-5">
            <div class="col-lg-8 pe-xl-5">
              <div class="row gy-lg-5 gy-4">
                @foreach ($getblogdetail as $blogdetail)
                <div class="col-lg-12">
                  <div class="blog-item">
                    <div class="thumb">
                      <img src="{{ asset('blogimage') }}/{{ $blogdetail->image }}" alt="" />
                    </div>
                    <div class="blog-content">
                      <span class="category">Business</span>
                      <h4 class="title">
                        {{ $blogdetail->title }}
                      </h4>
                      <div class="blog-meta d-flex flex-">
                        <ul class="meta d-flex flex-wrap justify-content-center">
                          <li>{{ $blogdetail->author }}</li>
                          <li>|</li>
                          <li>{{ \Carbon\Carbon::parse($blogdetail->date)->format('j F, Y') }}</li>
                        </ul>
                      </div>
                      <p class="para">
                        {!! $blogdetail->description !!}
                      </p>
                    </div>
                  </div>
                </div>
                @endforeach
                
              </div>
            </div>
  
            <div class="col-lg-4">
              <div class="row gy-lg-5 gy-4">
                <div class="col-lg-12">
                  <div class="right-sidebar">
                    <h5 class="sidebar-title">Search</h5>
                    <form action="#" class="search-box-two">
                      <input
                        type="text"
                        class="form-control form--control"
                        placeholder="Search key word"
                      />
                      <button type="submit"><i class="las la-search"></i></button>
                    </form>
                  </div>
                </div>
                
                <div class="col-lg-12">
                  <div class="blog-post">
                    <h5 class="sidebar-title">Popular News</h5>
                    <div class="latest-blog-wrapper d-flex flex-column flex-wrap">
                      <div class="row gy-2">
                        @foreach ($frontblog as $item)
                        <div class="col-sm-12">
                          <div class="latest-post d-flex flex-wrap">
                            <div class="latest-thumb flex-shrink-0">
                              <a href="{{ route('getBlog') }}"
                                ><img
                                  src="{{ asset('blogimage') }}/{{ $item->image }}"
                                  alt=""
                              /></a>
                            </div>
                            <div class="blog-content flex-grow-1 latest-blog">
                              <h3 class="title">
                                <a href="{{ route('getBlogDetail', $item->slug) }}"
                                  >{{ $item->title }}.</a
                                >
                              </h3>
                              <ul class="name-date d-flex flex-wrap">
                                <li>{{ $item->author }}</li>
                                <li>|</li>
                                <li>{{ \Carbon\Carbon::parse($item->date)->format('j F, Y') }}</li>
                              </ul>
                            </div>
                          </div>
                        </div>
                        @endforeach
                      </div>
                    </div>
                  </div>
                </div>
                
              </div>
            </div>
          </div>
        </div>
      </section>
    
    <!--========================Blogs ends here==========-->  
    <!-- ==================== CTA Start Here ==================== -->
   @include('front.partials.cta')
    <!-- ==================== CTA End Here ==================== -->

    <!-- ==================== Footer End Here ==================== -->
   @include('front.partials.footer')

    <div class="two"></div>
    <!-- ==================== Footer End Here ==================== -->

    <!-- ==================== Scroll to Top End Here ==================== -->
    <a class="scroll-top"><i class="fas fa-angle-double-up"></i></a>
    <!-- ==================== Scroll to Top End Here ==================== -->

  @include('front.partials.scripts')
  </body>
</html>
