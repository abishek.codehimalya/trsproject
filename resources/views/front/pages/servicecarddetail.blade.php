<!DOCTYPE html>
<html lang="en">
 @include('front.partials.styles')
  <body>
    <!--==================== Preloader Start ====================-->
    <div class="preloder" id="page-preloader">
      <div class="circle"></div>
    </div>
    <!--==================== Preloader End ====================-->

    <!-- ==================== Header Start Here ==================== -->
 @include('front.partials.header')
    <!-- ==================== Header End Here ==================== -->

    <!-- ==================== Banner Start Here ==================== -->
    <section class="breadcumb-section bg-overlay-one" style="background-image: url(/assets/frontend/images/breadcumb/breadcumb-img.png);">
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <div class="breadcumb text-center">
            <h1 class="breadcumb-title text-white">SERVICES DETAILS</h1>
            <ul class="breadcumb-list d-flex justify-content-center">
              <li><a href="{{ route('getHome') }}"> Home </a></li>
              <li><span> - </span></li>
              <li><a href="{{ route('getService') }}">Services</a></li>
              <li><span> - </span></li>
              <li style="color: rgb(5, 54, 133)">Services Details</li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </section>
    <!-- ==================== Banner End Here ==================== -->

   <!-- ==================== Service Detials Start Here ==================== -->
   <section class="service-details pt-120">
    <div class="container">
      <div class="row justify-content-center">
        @foreach ($getcard as $detail)
        <div class="col-lg-8">
          <div class="service-details-content">
            <div class="thumb">
              {{-- <img src="{{ asset('servicecardimage') }}/{{ $detail->image }}"
                alt=""
              /> --}}
            </div>
            <div class="icon-title d-flex align-items-center">
              <div class="icon flex-shrink-0">
                <img
                  src="{{ asset('assets/frontend/images/service-details/service-detials-icon.png') }}"
                  alt=""
                />
              </div>
              <h4 class="cmn-title flex-grow-1">{{ $detail->title1 }}</h4>
            </div>
            <p class="para">
            {!! $detail->desc1 !!}
            </p>
          </div>
        </div>
        @endforeach
      </div>
    </div>
  </section>
  <!-- ==================== Service Detials End Here ==================== -->

      <!-- ==================== CTA Start Here ==================== -->
   @include('front.partials.cta')
    <!-- ==================== CTA End Here ==================== -->

    <!-- ==================== Footer End Here ==================== -->
   @include('front.partials.footer')

    <div class="two"></div>
    <!-- ==================== Footer End Here ==================== -->

    <!-- ==================== Scroll to Top End Here ==================== -->
    <a class="scroll-top"><i class="fas fa-angle-double-up"></i></a>
    <!-- ==================== Scroll to Top End Here ==================== -->

  @include('front.partials.scripts')
  </body>
</html>
