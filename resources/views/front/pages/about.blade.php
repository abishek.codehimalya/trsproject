<!DOCTYPE html>
<html lang="en">
 @include('front.partials.styles')
  <body>
    <!--==================== Preloader Start ====================-->
    <div class="preloder" id="page-preloader">
      <div class="circle"></div>
    </div>
    <!--==================== Preloader End ====================-->

    <!-- ==================== Header Start Here ==================== -->
 @include('front.partials.header')
    <!-- ==================== Header End Here ==================== -->

    <!-- ==================== Banner Start Here ==================== -->
    <section
    class="breadcumb-section bg-overlay-one" style="background-image: url(/assets/frontend/images/breadcumb/breadcumb-img.png);">
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <div class="breadcumb text-center">
            <h1 class="breadcumb-title text-white">ABOUT COMPANY</h1>
            <ul class="breadcumb-list d-flex justify-content-center">
              <li><a href="../index.html"> Home </a></li>
              <li><span> - </span></li>
              <li style="color:rgb(5, 54, 133) ">About</li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </section>
    <!-- ==================== Banner End Here ==================== -->

    <!-- ====================  About Start Here ==================== -->
  {{-- @include('front.partials.about') --}}
  <section class="about py-120">
    <div class="container">
      <div class="row gy-4">
        <div class="col-lg-6">
          <div class="about-left">
            <div class="thumb">
              <img src="{{ asset('assets/frontend/images/home-1/about-left.png') }}" alt="" />
              <div class="experience-time bg--base">
                <h4 class="time text-white">10<span>years</span></h4>
                <h6 class="experience text-white">EXPERIENCES</h6>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-6">
          @foreach ($offers as $offer)
          <div class="about-right ps-lg-5">
            <h2 class="title">
              {{ $offer->title }}
            </h2>
            <p class="para">
              {!! Str::limit($offer->description,1000) !!}
            </p>
            {{-- <div class="about-btn">
              <a href="{{ route('getAbout') }}" class="btn--base style-three"
                >GET STARTED</a
              >
            </div> --}}
          </div>
          @endforeach

        </div>
      </div>
    </div>
  </section>
    <!-- ====================  About End Here ==================== -->

    <!-- ==================== Services Start Here ==================== -->
    @include('front.partials.service')
    <!-- ==================== Services End Here ==================== -->

    <!-- ==================== CEO Testimonial Start Here ==================== -->
    <section class="ceo-testi pt-120 pb-md-60 bg-overlay-one">
      <div class="container">
        <div class="row gy-sm-5 gy-4">
          <div class="col-sm-5">
            <div class="ceo-thumb">
              <img src=" {{ asset(getConfiguration('ceo_image')) }}" alt="" />
            </div>
          </div>
          <div class="col-sm-7">
            <div class="ceo-right-content ps-xl-5 ps-lg-4 ps-md-3">
              <div class="name-designation">
                <h4 class="name"> {{ getConfiguration('ceo_name') }}</h4>
                <span class="designation"> {{ getConfiguration('ceo_title') }}</span>
                <img src="assets/images/home-1/ceo-quate.png" alt="" />
              </div>
              <p class="para">
                {!! getConfiguration('ceo_desc') !!}
              </p>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- ==================== CEO Testimonial End Here ==================== -->

    <!-- ==================== Team Start Here ==================== -->
    <section class="blog pt-md-60 pb-120">
        <div class="container">
          <div class="row">
            <div class="col-lg-12">
              <div class="section-heading">
                <h3 class="title">Our Team Members</h3>
                <p class="para">
                  We're here with you to analyzing your business according to
                  different points of view and guide you in your business.
                </p>
              </div>
            </div>
          </div>
          <div class="row gy-4 justify-content-center">
            @foreach ($teams as $team)
            <div class="col-lg-4 col-md-6">
              <div class="blog-item">
                <div class="thumb">
                  <img src="{{ asset('teamimage') }}/{{ $team->image }}" alt="" />
                </div>
                <div class="blog-content pt-3">
                  <!-- <span class="category">Business</span> -->
                  <h4 class="title m-2">
                    <a class="text-center">{{ $team->name }}</a>
                  </h4>
                  <div class="blog-meta d-flex justify-content-center flex-wrap">
                    <ul class="meta">
                      <li class="text-center">{{ $team->title }}</li>
                    </ul>
                    <!-- <div class="blog-share">
                      <a href="javascript: void(0)"
                        ><i class="fas fa-share-alt"></i
                      ></a>
                      <div class="popup-share-icons">
                        <ul class="popup-icons d-flex flex-wrap">
                          <li>
                            <a href="#"><i class="fab fa-facebook-f"></i></a>
                          </li>
                          <li>
                            <a href="#"><i class="fab fa-twitter"></i></a>
                          </li>
                          <li>
                            <a href="#"><i class="fab fa-linkedin-in"></i></a>
                          </li>
                          <li>
                            <a href="#"><i class="fab fa-instagram"></i></a>
                          </li>
                        </ul>
                      </div>
                    </div> -->
                  </div>
                </div>
              </div>
            </div>
            @endforeach
          </div>
        </div>
      </section>
    <!-- ==================== Team End Here ==================== -->
    <!-- ==================== CTA Start Here ==================== -->
   @include('front.partials.cta')
    <!-- ==================== CTA End Here ==================== -->

    <!-- ==================== Footer End Here ==================== -->
   @include('front.partials.footer')

    <div class="two"></div>
    <!-- ==================== Footer End Here ==================== -->

    <!-- ==================== Scroll to Top End Here ==================== -->
    <a class="scroll-top"><i class="fas fa-angle-double-up"></i></a>
    <!-- ==================== Scroll to Top End Here ==================== -->

  @include('front.partials.scripts')
  </body>
</html>
