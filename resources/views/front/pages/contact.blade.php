<!DOCTYPE html>
<html lang="en">
 @include('front.partials.styles')
  <body>
    <!--==================== Preloader Start ====================-->
    <div class="preloder" id="page-preloader">
      <div class="circle"></div>
    </div>
    <!--==================== Preloader End ====================-->

    <!-- ==================== Header Start Here ==================== -->
 @include('front.partials.header')
    <!-- ==================== Header End Here ==================== -->

    <!-- ==================== Banner Start Here ==================== -->
    <section class="breadcumb-section bg-overlay-one" style="background-image: url(/assets/frontend/images/breadcumb/breadcumb-img.png);">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <div class="breadcumb text-center">
              <h1 class="breadcumb-title text-white">CONTACT WITH US</h1>
              <ul class="breadcumb-list d-flex justify-content-center">
                <li><a href="{{ route('getHome') }}"> Home </a></li>
                <li><span> - </span></li>
                <li style="color:rgb(5, 54, 133) ">CONTACT US</li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- ==================== Banner End Here ==================== -->

    <!-- ==================== Contact Map Start Here ==================== -->
    <div class="contact-map pt-120 pb-60">
        <div class="container">
          <div class="row">
            <div class="col-lg-12">
              <div class="map">
                <iframe
                  class="w-100"
                  src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d29186.92013262286!2d90.36077841204798!3d23.87667185261947!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3755c5d05e7074dd%3A0xd1c58803049f00c7!2sUttara%2C%20Dhaka!5e0!3m2!1sen!2sbd!4v1651120990016!5m2!1sen!2sbd"
                ></iframe>
              </div>
            </div>
          </div>
        </div>
      </div>
  
      <section class="contact-address mt-60 pb-60">
        <div class="container">
          <div class="row gy-4 justify-content-center">
            <div class="col-lg-4 col-md-4">
              <div class="address-item text-center">
                <div class="thumb">
                  <img src="../assets/images/contact/location.png" alt="" />
                </div>
                <div class="content">
                  <h5 class="title">Office Address</h5>
                  <p class="para">
                    {{ getConfiguration('address') }}
                  </p>
                </div>
              </div>
            </div>
            <div class="col-lg-4 col-md-4">
              <div class="address-item text-center">
                <div class="thumb">
                  <img src="../assets/images/contact/phone-book.png" alt="" />
                </div>
                <div class="content">
                  <h5 class="title">Phone Number</h5>
                  <p class="para"><a href="tel:"> {{ getConfiguration('primary_phone') }}</a></p>
                  <p class="para"><a href="tel:"> {{ getConfiguration('secondary_phone') }}</a></p>
                </div>
              </div>
            </div>
            <div class="col-lg-4 col-md-4">
              <div class="address-item text-center">
                <div class="thumb">
                  <img src="../assets/images/contact/mail.png" alt="" />
                </div>
                <div class="content">
                  <h5 class="title">Email Address</h5>
                  <p class="para">
                    <a href="mailto:"><span class="__cf_email__"
                        data-cfemail="1672737b7975797b6677786f56717b777f7a3875797b"> {{ getConfiguration('primary_mail') }}</span>
                    </a>
                  </p>
                  <p class="para">
                    <a href="mailto:"
                      ><span
                        class="__cf_email__"
                        data-cfemail="81e4f9ecf1ede4b5b4b6c1e6ece0e8edafe2eeec"
                        > {{ getConfiguration('secondary_mail') }}</span
                      ></a
                    >
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
  
      <div class="consultation-form pt-60 pb-60">
        <div class="container">
          <div class="row justify-content-center">
            <div class="col-lg-8">
              <form class="contact-form" autocomplete="off" method="POST" action="{{ route('contactus.store') }}">
                @csrf
                <h3 class="title">Request For Free Consultation</h3>
                <div class="row">
                  <div class="col-lg-12">
                    <input
                      type="email" name="useremail"
                      class="form-control form--control"
                      placeholder="Email"
                      required=""
                    />
                  </div>
                  <div class="col-lg-12">
                    <input
                      type="text" name="subject"
                      class="form-control form--control"
                      placeholder="Subject"
                      required=""
                    />
                  </div>
                  <div class="col-lg-12">
                    <textarea name="message"
                      class="form-control form--control"
                      placeholder="Message"
                    ></textarea>
                  </div>
                  <div class="col-lg-12">
                    <div class="contact-button">
                      <button type="submit" class="btn--base style-three w-100">
                        SEND MESSAGES
                      </button>
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
      <!-- ==================== Contact Map End Here ==================== -->

    <!-- ==================== CTA Start Here ==================== -->
   @include('front.partials.cta')
    <!-- ==================== CTA End Here ==================== -->

    <!-- ==================== Footer End Here ==================== -->
   @include('front.partials.footer')

    <div class="two"></div>
    <!-- ==================== Footer End Here ==================== -->

    <!-- ==================== Scroll to Top End Here ==================== -->
    <a class="scroll-top"><i class="fas fa-angle-double-up"></i></a>
    <!-- ==================== Scroll to Top End Here ==================== -->

  @include('front.partials.scripts')
  </body>
</html>
