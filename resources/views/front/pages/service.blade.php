<!DOCTYPE html>
<html lang="en">
 @include('front.partials.styles')
  <body>
    <!--==================== Preloader Start ====================-->
    <div class="preloder" id="page-preloader">
      <div class="circle"></div>
    </div>
    <!--==================== Preloader End ====================-->

    <!-- ==================== Header Start Here ==================== -->
 @include('front.partials.header')
    <!-- ==================== Header End Here ==================== -->

    <!-- ==================== Banner Start Here ==================== -->
    <section class="breadcumb-section bg-overlay-one" style="background-image: url(/assets/frontend/images/breadcumb/breadcumb-img.png);">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <div class="breadcumb text-center">
              <h1 class="breadcumb-title text-white">OUR AWESOME SERVICES</h1>
              <ul class="breadcumb-list d-flex justify-content-center">
                <li><a href="{{ route('getHome') }}"> Home </a></li>
                <li><span> - </span></li>
                <li style="color: rgb(5, 54, 133)">Services</li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- ==================== Banner End Here ==================== -->

   <!-- ==================== Service Start Here ==================== -->
   <section class="home-two service pt-120 pb-60">
    <div class="container">
      <div class="row justify-content-center gy-4">
        @foreach ($getallservice as $service)
        <div class="col-lg-4 col-md-6">
          <div class="service-item">
            <div class="title-icon d-flex flex-wrap">
              <div class="icon">
                <img
                  src="{{ asset('mainserviceimage') }}/{{ $service->image }}"
                  alt=""
                />
              </div>
              <h5 class="title">
                <a href="{{ route('getServiceDetail', $service->slug) }}">{{ $service->title }}</a>
              </h5>
            </div>
            <p class="para">
              {!! $service->description !!}
            </p>
          </div>
        </div>
        @endforeach
      </div>
    </div>
  </section>
  <!-- ==================== Service End Here ==================== -->

  <!-- ==================== accordion End Here ==================== -->
  @include('front.partials.service')
  {{-- <section class="services-page accordion pt-60 pb-60">
    <div class="container">
      <div class="row gy-4">
        <div class="col-xl-5">
          <div class="service-left">
            <div class="section-heading style-two">
              <h3 class="title">What`S You Want To Know About Our Service</h3>
              <p class="para">
                We're here with you to analyzing your business according to
                different points of view and guide you in your business.
              </p>
            </div>
            <div class="accordion custom--accordion" id="accordionExample">
              <div class="accordion-item">
                <h2 class="accordion-header" id="headingOne">
                  <button
                    class="accordion-button"
                    type="button"
                    data-bs-toggle="collapse"
                    data-bs-target="#collapseOne"
                    aria-expanded="true"
                    aria-controls="collapseOne"
                  >
                    Do we really need a business plan?
                  </button>
                </h2>
                <div
                  id="collapseOne"
                  class="accordion-collapse collapse show"
                  aria-labelledby="headingOne"
                  data-bs-parent="#accordionExample"
                >
                  <div class="accordion-body">
                    <p>
                      On the off chance that you maintain a business, saying
                      OK is presumably protected. The last option is a more
                      point by point report filling in as an everyday guide,
                      itemizing the strategies supporting your general
                      technique.
                    </p>
                  </div>
                </div>
              </div>
              <div class="accordion-item">
                <h2 class="accordion-header" id="headingTwo">
                  <button
                    class="accordion-button collapsed"
                    type="button"
                    data-bs-toggle="collapse"
                    data-bs-target="#collapseTwo"
                    aria-expanded="false"
                    aria-controls="collapseTwo"
                  >
                    What makes your business plans so special?
                  </button>
                </h2>
                <div
                  id="collapseTwo"
                  class="accordion-collapse collapse"
                  aria-labelledby="headingTwo"
                  data-bs-parent="#accordionExample"
                >
                  <div class="accordion-body">
                    <p>
                      Above all, they work. In a world wherein less than 1 out
                      of 250 strategies at any point raise funding, we are
                      glad to report that about portion of our arrangements
                      have raised capital, and a decent piece of the rest
                      brought about an obtaining or as of now have
                      responsibilities for speculation from earlier round
                      financial backers.
                    </p>
                  </div>
                </div>
              </div>
              <div class="accordion-item">
                <h2 class="accordion-header" id="headingThree">
                  <button
                    class="accordion-button collapsed"
                    type="button"
                    data-bs-toggle="collapse"
                    data-bs-target="#collapseThree"
                    aria-expanded="false"
                    aria-controls="collapseThree"
                  >
                    What do investors look for in a business plan?
                  </button>
                </h2>
                <div
                  id="collapseThree"
                  class="accordion-collapse collapse"
                  aria-labelledby="headingThree"
                  data-bs-parent="#accordionExample"
                >
                  <div class="accordion-body">
                    <p>No, we are management consultants, not investors.</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-xl-7">
          <div class="home-two-form ps-lg-5">
            <div class="background-thumb">
              <img src="assets/frontend/images/home-2/accordion.png" alt="" />
            </div>
            <div class="form">
              <div class="consulting-form">
                <form action="#" autocomplete="off">
                  <h5 class="title">Request A Call Back</h5>
                  <input
                    type="email"
                    class="form-control form--control"
                    placeholder="Email Address"
                    required=""
                  />
                  <input
                    type="text"
                    class="form-control form--control"
                    placeholder="Subject"
                    required=""
                  />
                  <textarea
                    class="form-control form--control"
                    placeholder="Messages"
                  ></textarea>
                  <button
                    type="submit"
                    class="btn--base style-three mt-4 w-100"
                  >
                    SEND MESSAGES
                  </button>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section> --}}
  <!-- ==================== accordion End Here ==================== -->

      <!-- ==================== CTA Start Here ==================== -->
   @include('front.partials.cta')
    <!-- ==================== CTA End Here ==================== -->

    <!-- ==================== Footer End Here ==================== -->
   @include('front.partials.footer')

    <div class="two"></div>
    <!-- ==================== Footer End Here ==================== -->

    <!-- ==================== Scroll to Top End Here ==================== -->
    <a class="scroll-top"><i class="fas fa-angle-double-up"></i></a>
    <!-- ==================== Scroll to Top End Here ==================== -->

  @include('front.partials.scripts')
  </body>
</html>
