<section class="features">
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <div class="features-content">
            <div class="row justify-content-center">
              @foreach ($features as $feature)
              <div class="col-lg-4 col-md-4">

                <div class="features-item with-border shadow bg-white">
                  <div class="title-icon d-flex flex-wrap">
                    <div class="icon">
                      <img src ="{{asset('servicecardimage')}}/{{$feature->image1}}" alt="icon"/>
                    </div>
                    <h5 class="title">
                      <a href=" {{ route('getscDetail', $feature->slug1) }}">{{ $feature->title1 }}</a>
                    </h5>
                  </div>
                  <p class="para">
                   {!! Str::limit($feature->desc1, 100) !!}
                  </p>
                </div>
              </div>
              {{-- -------card two -----------}}
              <div class="col-lg-4 col-md-4">

                <div class="features-item with-border shadow bg-white">
                  <div class="title-icon d-flex flex-wrap">
                    <div class="icon">
                      <img src ="{{asset('servicecardimage')}}/{{$feature->image2}}" alt="icon"/>
                    </div>
                    <h5 class="title">
                      <a href="{{ route('getsctwo', $feature->slug2) }}">{{ $feature->title2 }}</a>
                    </h5>
                  </div>
                  <p class="para">
                   {!! Str::limit($feature->desc2, 100) !!}
                  </p>
                </div>
              </div>
              {{-- -------card three -----------}}

              <div class="col-lg-4 col-md-4">

                <div class="features-item with-border shadow bg-white">
                  <div class="title-icon d-flex flex-wrap">
                    <div class="icon">
                      <img src ="{{asset('servicecardimage')}}/{{$feature->image3}}" alt="icon"/>
                    </div>
                    <h5 class="title">
                      <a href="{{ route('getscthree', $feature->slug3) }}">{{ $feature->title3 }}</a>
                    </h5>
                  </div>
                  <p class="para">
                   {!! Str::limit($feature->desc3, 100) !!}
                  </p>
                </div>
              </div>
              @endforeach

            </div>
          </div>
        </div>
      </div>
    </div>
  </section>