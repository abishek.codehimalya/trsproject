<section class="accordion py-120">
    <div class="container">
      <div class="row gy-4">
        <div class="col-lg-6">
          <div class="service-left">
            <div class="section-heading style-two">
              <h3 class="title">FAQs</h3>
              <p class="para">
                We're here with you to analyzing your accounting according to
                different points of view and guide you in your business.
              </p>
            </div>
            <div class="accordion custom--accordion" id="accordionExample">
              @foreach ($faqs as $item)
              <div class="accordion-item">
                <h2 class="accordion-header" id="headingOne{{ $item->id }}">
                  <button
                    class="accordion-button collapsed"
                    type="button"
                    data-bs-toggle="collapse"
                    data-bs-target="#collapseOne{{ $item->id }}"
                    aria-expanded="false"
                    aria-controls="collapseOne{{ $item->id }}"
                  >
                    {!! $item->question !!}
                  </button>
                </h2>
                <div
                  id="collapseOne{{ $item->id }}"
                  class="accordion-collapse collapse "
                  aria-labelledby="headingOne{{ $item->id }}"
                  data-bs-parent="#accordionExample"
                >
                  <div class="accordion-body">
                    <p>
                      {!! $item->answer !!}
                    </p>
                  </div>
                </div>
              </div>
              @endforeach

              {{-- <div class="accordion-item">
                <h2 class="accordion-header" id="headingTwo">
                  <button
                    class="accordion-button collapsed"
                    type="button"
                    data-bs-toggle="collapse"
                    data-bs-target="#collapseTwo"
                    aria-expanded="false"
                    aria-controls="collapseTwo"
                  >
                    Tax Prepration
                  </button>
                </h2>
                <div
                  id="collapseTwo"
                  class="accordion-collapse collapse"
                  aria-labelledby="headingTwo"
                  data-bs-parent="#accordionExample"
                >
                  <div class="accordion-body">
                    <p>
                      Hire a professional firm to handle your taxes. We
                      provide professional and timely services.
                    </p>
                  </div>
                </div>
              </div>
              <div class="accordion-item">
                <h2 class="accordion-header" id="headingThree">
                  <button
                    class="accordion-button collapsed"
                    type="button"
                    data-bs-toggle="collapse"
                    data-bs-target="#collapseThree"
                    aria-expanded="false"
                    aria-controls="collapseThree"
                  >
                    Financial Statement
                  </button>
                </h2>
                <div
                  id="collapseThree"
                  class="accordion-collapse collapse"
                  aria-labelledby="headingThree"
                  data-bs-parent="#accordionExample"
                >
                  <div class="accordion-body">
                    <p>
                      We can prepare or supervise the preparation of your
                      financial statements. Then fully compiled and reviewed
                      according to your needs.
                    </p>
                  </div>
                </div>
              </div> --}}
            </div>
          </div>
        </div>
        <div class="col-lg-6">
          <div class="service-right">
            <ul class="d-flex justify-content-start">
              <li>
                <img src="{{ asset('assets/frontend/images/home-1/accordion-img-01.png') }}" alt="" />
              </li>
              <li>
                <img src="{{ asset('assets/frontend/images/home-1/accordion-img-02.png') }}" alt="" />
              </li>
              <li>
                <img src="{{ asset('assets/frontend/images/home-1/accordion-img-03.png') }}" alt="" />
              </li>
              <li>
                <img
                  src="{{ asset('assets/frontend/images/home-1/accrodion-shape-01.png') }}"
                  alt=""
                />
              </li>
              <li>
                <img
                  src="{{ asset('assets/frontend/images/home-1/accrodion-shape-02.png') }}"
                  alt=""
                />
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </section>
