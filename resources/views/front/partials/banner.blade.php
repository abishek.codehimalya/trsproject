<section
class="banner bg-img bg-overlay-one"
style="background-image: url(assets/frontend/images/home-1/banner-img.png)">
<div class="container">
  <div class="row justify-content-center text-center">
    <div class="col-lg-12">
      <div class="banner-content">
        <h1 class="title">WE PROVIDE A BEST ACCOUNTING SERVICE</h1>
        <div class="banner-content__buttons">
          <ul class="buttons-list">
            <li class="buttons-list__item">
              <a href="{{ route('getContact') }}" class="btn--base">BOOK APPOINTMENT</a>
            </li>
            <li class="buttons-list__item">
              <a href="{{ route('getService') }}" class="btn--base style-two"
                >SERVICES</a
              >
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>
</section>