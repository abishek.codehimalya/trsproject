<section class="consulting-agency bg-overlay-one">
    <div class="consult pt-120 pb-md-60">
      <div class="container">
        <div class="row">
          <div class="col-lg-6">
            <div class="section-heading style-two">
              <h3 class="title" style="color: #14306f">
                Consult Us For Your Business.
              </h3>
              <p class="para" style="color: #14306f">
                We're here with you to analyzing your accounting according to
                different points of view and guide you in your business.
              </p>
            </div>
          </div>
          <div class="col-lg-6">
            <div class="form ps-lg-5">
              <div class="consulting-form">
                {{-- @include('backend.layouts.message') --}}
                <form action="{{ route('contactus.store') }}" autocomplete="off" method="POST" enctype="multipart/form-data">
                  @csrf
                  <h5 class="title">Request A Call Back</h5>
                  <input
                    type="email" name="useremail"
                    class="form-control form--control"
                    placeholder="Email Address"
                    required=""
                  />
                  
                  <input
                    type="text" name="subject"
                    class="form-control form--control"
                    placeholder="Subject"
                    required=""
                  />
                  <textarea name="message"
                    class="form-control form--control"
                    placeholder="Messages"
                  ></textarea>
                  <button
                    type="submit"
                    class="btn--base style-three mt-5 w-100"
                  >
                    SEND MESSAGES
                  </button>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>