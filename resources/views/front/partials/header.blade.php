<header class="header">
    <div class="header-top d-none d-lg-block">
      <div class="container">
        <div
          class="top-header-wrapper d-flex flex-wrap justify-content-between align-items-center"
        >
          <div class="top-contact">
            <ul
              class="top-contact-menu d-flex flex-wrap justify-content-between align-items-center"
            >
              <li class="item d-flex flex-wrap align-items-center">
                <div class="icon">
                  <i class="fas fa-map-marker-alt"></i>
                </div>
                <div class="text">
                  <p>{{ getConfiguration('address') }}</p>
                </div>
              </li>
              <li class="item d-flex flex-wrap align-items-center">
                <div class="icon">
                  <i class="fas fa-globe"></i>
                </div>
                <div class="text">
                  <p>{{ getConfiguration('website') }}</p>
                </div>
              </li>
            </ul>
          </div>
          <ul class="social-icons d-flex flex-wrap">
            <li>
            @if(!empty(getConfiguration('facebook')))
              <a href="{{ getConfiguration('facebook') }}" target="_blank" class="facebook"><i class="fab fa-facebook-f"></i></a>
            @endif
            </li>
            <li>
            @if(!empty(getConfiguration('twitter')))
              <a href="{{ getConfiguration('twitter') }}" target="_blank" class="twitter"><i class="fab fa-twitter"></i></a>
            @endif
            </li>
            <li>
            @if(!empty(getConfiguration('linkedin')))
              <a href="{{ getConfiguration('linkedin') }}" target="_blank" class="linkedIn"><i class="fab fa-linkedin-in"></i></a>
            @endif
            </li>
            <li>
            @if(!empty(getConfiguration('linkedin')))
              <a href="{{ getConfiguration('instagram') }}" target="_blank" class="instagram"><i class="fab fa-instagram"></i></a>
            @endif
            </li>
          </ul>
        </div>
      </div>
    </div>
    <div class="container">
      <div class="header-container">
        <div class="header-bottom">
          <nav class="navbar navbar-expand-lg navbar-light">
            <a class="navbar-brand logo" href="{{ route('getHome') }}"
              ><img src="{{ asset(getConfiguration('logo_image'))}}" alt="logo"
            /></a>
            <button
              class="navbar-toggler header-button"
              type="button"
              data-bs-toggle="collapse"
              data-bs-target="#navbarSupportedContent"
              aria-controls="navbarSupportedContent"
              aria-expanded="false"
              aria-label="Toggle navigation"
            >
              <i class="las la-bars"></i>
            </button>

            <div class="toggle-search-box">
              <div class="search-icon">
                <i class="las la-search"></i>
              </div>
              <div class="search-input">
                <form>
                  <input type="text" placeholder="Search..." />
                  <button type="submit"><i class="las la-search"></i></button>
                </form>
              </div>
            </div>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
              <ul class="navbar-nav nav-menu ms-auto">
                <li class="nav-item">
                  <a class="nav-link" href="{{ route('getHome') }}"> Home </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="{{ route('getAbout') }}">About</a>
                </li>
                <li class="nav-item dropdown">
                  <a
                    class="nav-link"
                    href="{{ route('getService') }}"
                    
                    data-bs-toggle="dropdown"
                    aria-expanded="false"
                  >
                    Service <i class="fa-solid fa-angle-down"></i> </a>
                  <ul class="dropdown-menu">
                    @foreach (getServices() as $item)   
                    <li>
                      <a
                        class="dropdown-item"
                        href="{{ route('getServiceDetail', $item->slug) }}"
                        >{{ $item->title }}</a
                      >
                    </li>
                    @endforeach
                  </ul>
                </li>

                <li class="nav-item">
                  <a class="nav-link" href="{{ route('getBlog') }}">News</a>
                </li>

                <li class="nav-item">
                  <a class="nav-link" href="{{ route('getContact') }}">Contact</a>
                </li>
              </ul>
            </div>
          </nav>
        </div>
      </div>
    </div>
  </header>