<section class="cta bg-overlay-one">
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <div class="cta-content">
            <div class="section-heading">
              <h3 class="title text-white">Our Latest News Update</h3>
              <p class="para text-white" style="word-break: normal;">
                We're here with you to analyzing your business according to
                      different points of view and guide you in your business.
              </p>
            </div>
            <form action="{{ route('store.contact') }}" autocomplete="off" method="POST" enctype="multipart/form-data">
              @csrf
              <div class="input--group">
                <input
                  type="email" name="useremail"
                  class="form-control form--control"
                  placeholder="Enter Email Address"
                  required=""
                />

                  @if (!empty($errors->all()))
                    @foreach ($errors->all() as $error)
                        <span style="color: white;">{{$error}}</span>
                    @endforeach
                  @endif

                <button type="submit">
                  <i class="lab la-telegram-plane"></i>
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </section>