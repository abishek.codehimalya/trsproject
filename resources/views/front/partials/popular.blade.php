<section class="popular-work pb-120 pt-md-60">
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <div class="section-heading">
            <h3 class="title">Let`s see Our Popular work</h3>
            <p class="para">
              We're here with you to analyzing your accounting according to
              different points of view and guide you in your business.
            </p>
          </div>
        </div>
      </div>
      <div class="row gy-4">
        @foreach ($populars as $popular)
        <div class="col-lg-4 col-sm-6">
          <div class="work-item">
            <div class="thumb">
              <img
                class="w-100"
                src="{{ asset('popularimage') }}/{{ $popular->image }}"
                alt=""
              />
              <div class="text">
                <h5 class="title">
                  <span
                    >{{ $popular->image_title }}</span
                  >
                </h5>
              </div>
            </div>
          </div>
        </div>
        @endforeach
      </div>
    </div>
  </section>