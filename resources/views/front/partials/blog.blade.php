<section class="blog pt-60 pb-120">
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <div class="section-heading">
            <h3 class="title">Our Latest News Update</h3>
            <p class="para">
              We're here with you to analyzing your accounting according to
              different points of view and guide you in your business.
            </p>
          </div>
        </div>
      </div>
      <div class="row gy-4 justify-content-center">
        @foreach ($frontblog as $blog)
        <div class="col-lg-4 col-md-6">
          <div class="blog-item">
            <div class="thumb">
              <img src="{{ asset('blogimage') }}/{{ $blog->image }}" alt="blog" />
            </div>
            <div class="blog-content">
              <span class="category">{{ $blog->name }}</span>
              <h4 class="title">
                <a href="{{ route('getBlogDetail', $blog->slug) }}"
                  >{{ $blog->title }}</a
                >
              </h4>
              <div class="blog-meta d-flex justify-content-between flex-wrap">
                <ul class="meta d-flex flex-wrap justify-content-center">
                  <li>By {{ $blog->author }}</li>
                  <li>|</li>
                  <li>{{ \Carbon\Carbon::parse($blog->date)->format('j F, Y')  }}</li>
                </ul>
                <div class="blog-share">
                  <a href="javascript: void(0)"
                    ><i class="fas fa-share-alt"></i
                  ></a>
                  <div class="popup-share-icons">
                    <ul class="popup-icons d-flex flex-wrap">
                      <li>
                        <a href="#"><i class="fab fa-facebook-f"></i></a>
                      </li>
                      <li>
                        <a href="#"><i class="fab fa-twitter"></i></a>
                      </li>
                      <li>
                        <a href="#"><i class="fab fa-linkedin-in"></i></a>
                      </li>
                      <li>
                        <a href="#"><i class="fab fa-instagram"></i></a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        @endforeach
      </div>
    </div>
  </section>