<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>TRS Partners Pvt Ltd</title>
    <!-- Favicon -->
    <link rel="icon" href="{{ asset(getConfiguration('fabicon_image'))}}" />
    <!-- bootstrap -->
    <link rel="stylesheet" href="{{ asset('assets/frontend/css/bootstrap.min.css') }}" />
    <!-- fontawesome -->
    <link rel="stylesheet" href="{{ asset('assets/frontend/css/fontawesome-all.min.css') }}" />
    <!-- line-awesome -->
    <link rel="stylesheet" href="{{ asset('assets/frontend/css/line-awesome.min.css') }}" />
    <!-- slick slider -->
    <link rel="stylesheet" href="{{ asset('assets/frontend/css/slick.css') }}" />
    <!-- magnific popup css -->
    <link rel="stylesheet" href="{{ asset('assets/frontend/css/magnific-popup.css') }}" />
    <!--Odometer css -->
    <link rel="stylesheet" href="{{ asset('assets/frontend/css/odometer.css') }}" />
    <!-- main css -->
<!--toastr-->
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">


    <link rel="stylesheet" href="{{ asset('assets/frontend/css/style.css') }}" />
  </head>