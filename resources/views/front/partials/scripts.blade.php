  <!-- jquery -->
  <script data-cfasync="false" src="../../cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script>
  <script src="{{ asset('assets/frontend/js/jquery-3.6.0.min.js') }}"></script>
  <!-- popper js -->
  <script src="{{ asset('assets/frontend/js/popper.min.js') }}"></script>
  <!-- bootstrap js -->
  <script src="{{ asset('assets/frontend/js/bootstrap.min.js') }}"></script>
  <!-- slick slider js -->
  <script src="{{ asset('assets/frontend/js/slick.min.js') }}"></script>
  <!-- magnific popup js -->
  <script src="{{ asset('assets/frontend/js/magnific-popup.min.js') }}"></script>
  <!-- odometer js -->
  <script src="{{ asset('assets/frontend/js/odometer.min.js') }}"></script>
  <!-- Viewport js -->
  <script src="{{ asset('assets/frontend/js/viewport.jquery.js') }}"></script>
  <!-- main js -->
  <script src="{{ asset('assets/frontend/js/main.js') }}"></script>


  <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
  <script>
        $(document).ready(function() {
            toastr.options.timeOut = 2000;
            @if (Session::has('error'))
                toastr.error('{{ Session::get('error') }}');
            @elseif(Session::has('success'))
                toastr.success('{{ Session::get('success') }}');
            @endif
        });
    </script>