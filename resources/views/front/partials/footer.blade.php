<footer class="footer-area">
    <div class="container footer-top py-120">
      <div class="row justify-content-center gy-5">
        <div class="col-xl-4 col-md-6 col-sm-8">
          <div class="footer-item">
            <h4 class="common-footer">Contact Information</h4>
            <ul class="footer-menu">
              <li class="d-flex flex-wrap align-items-center">
                <div class="icon"><i class="las la-map-marker-alt"></i></div>
                <div class="content">
                  <p class="text">{{ getConfiguration('address') }}</p>
                </div>
              </li>
              <li class="d-flex flex-wrap align-items-center">
                <div class="icon"><i class="las la-phone-volume"></i></div>
                <div class="content">
                  <a href="tel:{{ getConfiguration('primary_phone') }}" class="text">{{ getConfiguration('primary_phone') }}</a>
                </div>
              </li>
              <li class="d-flex flex-wrap align-items-center">
                <div class="icon"><i class="fas fa-paper-plane"></i></div>
                <div class="content">
                  <a href="mailto:{{ getConfiguration('website') }}" class="text">{{ getConfiguration('website') }}</a>
                </div>
              </li>
            </ul>
          </div>
        </div>
        <div class="col-xl-2 col-md-6 col-sm-4">
          <div class="footer-item">
            <h4 class="common-footer">Quick Links</h4>
            <ul class="footer-menu">
              <li><a href="{{ route('getAbout') }}"> About Us</a></li>
              <li><a href="{{ route('getService') }}"> Our Service</a></li>
              <li><a href="{{ route('getBlog') }}">News</a></li>
              <li><a href="{{ route('getContact') }}">Contact Us</a></li>
            </ul>
          </div>
        </div>
        <div class="col-xl-2 col-md-6 col-sm-6">
          <div class="footer-item">
            <h4 class="common-footer">Service</h4>
            <ul class="footer-menu">
          @foreach (footerService() as $service)
          <li><a href="{{ route('getServiceDetail', $service->slug) }}">{{ $service->title }}</a></li>
          @endforeach

            </ul>
          </div>
        </div>
        <div class="col-xl-4 col-md-6 col-sm-6">
          <div class="footer-item">
            <h4 class="common-footer">NEWS LETTER</h4>
            <div class="footer-buttons pt-3">
              <form action="{{ route('store.contact') }}" method="POST" autocomplete="off">
                @csrf
                <input
                  type="email" name="useremail"
                  class="form-control form--control"
                  placeholder="Enter Your Email"
                  required=""
                />
                @if (!empty($errors->all()))
                  @foreach ($errors->all() as $error)
                    <span style="color: white;">{{$error}}</span>
                  @endforeach
                @endif
                <button type="submit" class="btn--base w-100 mt-3">
                  SUBSCRIBE NOW!
                </button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- bottom Footer -->
    <div class="bottom-footer">
      <div class="container">
        <div class="row gy-3">
          <div class="col-md-12 text-center">
            <div class="bottom-footer-text text-white">
              Copyright &copy; 2022. All Rights Reserved By TRS Partners Pvt. Ltd.
            </div>
          </div>
        </div>
      </div>
    </div>
  </footer>