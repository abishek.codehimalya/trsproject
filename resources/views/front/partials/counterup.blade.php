<section
class="counterup py-120 bg-img"
style="background-image: url(assets/frontend/images/home-1/counter-up.png)">
<div class="counter-play-button">
  <a
    class="popup-video"
    href="https://www.youtube.com/watch?v=D0Ekx9YK-I0"
    ><i class="fas fa-play"></i
  ></a>
</div>
<div class="background-left"></div>
<div class="container">
  <div class="row">
    <div class="col-lg-6">
      @foreach ($countups as $count)
      <div class="counterup-content">
        <div class="section-heading style-two">
          <h3 class="title text-white">
            Save Time And Money With A Top IT Solution Agency
          </h3>
          <p class="para text-white">
            We're here with you to analyzing your business according to
            different points of view and guide you in your business.
          </p>
        </div>
        <div class="row g-4">
          <div class="col-6 col-sm-3 col-lg-6">
            <div class="counterup-item">
              <div class="number">
                <h2 class="title">
                  <span class="odometer" data-odometer-final="{{ $count->count_one }}">0</span>K
                </h2>
              </div>
              <p class="para">Positive Feedback</p>
            </div>
          </div>
          <div class="col-6 col-sm-3 col-lg-6">
            <div class="counterup-item">
              <div class="number">
                <h2 class="title">
                  <span class="odometer" data-odometer-final="{{ $count->count_two }}">0</span
                  >M
                </h2>
              </div>
              <p class="para">Positive Feedback</p>
            </div>
          </div>
          <div class="col-6 col-sm-3 col-lg-6">
            <div class="counterup-item">
              <div class="number">
                <h2 class="title">
                  <span class="odometer" data-odometer-final="{{ $count->count_three }}">0</span>
                </h2>
              </div>
              <p class="para">Brunch Office</p>
            </div>
          </div>
          <div class="col-6 col-sm-3 col-lg-6">
            <div class="counterup-item">
              <div class="number">
                <h2 class="title">
                  <span class="odometer" data-odometer-final="{{ $count->count_four }}">0</span
                  >+
                </h2>
              </div>
              <p class="para">Expert Advisor</p>
            </div>
          </div>
        </div>
      </div>
      @endforeach
    </div>
    <div class="col-lg-3"></div>
  </div>
</div>
</section>