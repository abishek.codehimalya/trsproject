<section class="ceo-testi pt-120 pb-md-60 bg-overlay-one">
    <div class="container">
      <div class="row gy-sm-5 gy-4">
        <div class="col-sm-5">
          <div class="ceo-thumb">
            <img src=" {{ asset(getConfiguration('ceo_image')) }}" alt="" />
          </div>
        </div>
        <div class="col-sm-7">
          <div class="ceo-right-content ps-xl-5 ps-lg-4 ps-md-3">
            <div class="name-designation">
              <h4 class="name"> {{ getConfiguration('ceo_name') }}</h4>
              <span class="designation"> {{ getConfiguration('ceo_title') }}</span>
              <img src="assets/images/home-1/ceo-quate.png" alt="" />
            </div>
            <p class="para">
              {!! Str::limit(getConfiguration('ceo_desc'),1000) !!}
            </p>
          </div>
        </div>
      </div>
    </div>
  </section>