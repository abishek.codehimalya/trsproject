<section class="about py-120">
    <div class="container">
      <div class="row gy-4">
        <div class="col-lg-6">
          <div class="about-left">
            <div class="thumb">
              <img src="{{ asset('assets/frontend/images/home-1/about-left.png') }}" alt="" />
              <div class="experience-time bg--base">
                <h4 class="time text-white">10<span>years</span></h4>
                <h6 class="experience text-white">EXPERIENCES</h6>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-6">
          @foreach ($offers as $offer)
          <div class="about-right ps-lg-5">
            <h2 class="title">
              {{ $offer->title }}
            </h2>
            <p class="para">
              {!! Str::limit($offer->description,1000) !!}
            </p>
            <div class="about-btn">
              <a href="{{ route('getAbout') }}" class="btn--base style-three"
                >GET STARTED</a
              >
            </div>
          </div>
          @endforeach

        </div>
      </div>
    </div>
  </section>