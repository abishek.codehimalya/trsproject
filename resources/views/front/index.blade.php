<!DOCTYPE html>
<html lang="en">
 @include('front.partials.styles')
  <body>
    <!--==================== Preloader Start ====================-->
    <div class="preloder" id="page-preloader">
      <div class="circle"></div>
    </div>
    <!--==================== Preloader End ====================-->

    <!-- ==================== Header Start Here ==================== -->
 @include('front.partials.header')
    <!-- ==================== Header End Here ==================== -->

    <!-- ==================== Banner Start Here ==================== -->
  @include('front.partials.banner')
    <!-- ==================== Banner End Here ==================== -->

    <!-- ==================== Features Start Here ==================== -->
   @include('front.partials.features')
    <!-- ==================== Features End Here ==================== -->

    <!-- ====================  About Start Here ==================== -->
  @include('front.partials.about')
    <!-- ====================  About End Here ==================== -->

    <!-- ====================  Counterup Start Here ==================== -->
 @include('front.partials.counterup')
    <!-- ====================  Counterup End Here ==================== -->

    <!-- ====================  Team Start Here ==================== -->
    <!-- <section
      class="team py-120 bg-img bg-overlay-one"
      style="background-image: url(assets/images/team/expert-bg.png)">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <div class="section-heading">
              <h3 class="title">Meet With Our Expert</h3>
              <p class="para">
                We're here with you to analyzing your business according to
                different points of view and guide you in your business.
              </p>
            </div>
          </div>
        </div>
        <div class="row justify-content-center gy-5">
          <div class="col-md-4 col-sm-6">
            <div class="team-item">
              <div class="thumb">
                <img src="assets/images/team/expert-04.png" alt="" />
                <ul
                  class="social-icons justify-content-center style-two bg-overlay-two d-flex flex-wrap"
                >
                  <li>
                    <a href="#"><i class="fab fa-facebook-f"></i></a>
                  </li>
                  <li>
                    <a href="#"><i class="fab fa-twitter"></i></a>
                  </li>
                  <li>
                    <a href="#"><i class="fab fa-instagram"></i></a>
                  </li>
                </ul>
              </div>
              <div class="content text-center">
                <h5 class="name">
                  <a href="team-details.html">Martin Chuso</a>
                </h5>
                <span class="designation">Business Expert</span>
              </div>
            </div>
          </div>
          <div class="col-md-4 col-sm-6">
            <div class="team-item">
              <div class="thumb">
                <img src="assets/images/team/expert-05.png" alt="" />
                <ul
                  class="social-icons justify-content-center style-two bg-overlay-two d-flex flex-wrap"
                >
                  <li>
                    <a href="#"><i class="fab fa-facebook-f"></i></a>
                  </li>
                  <li>
                    <a href="#"><i class="fab fa-twitter"></i></a>
                  </li>
                  <li>
                    <a href="#"><i class="fab fa-instagram"></i></a>
                  </li>
                </ul>
              </div>
              <div class="content text-center">
                <h5 class="name">
                  <a href="team-details.html">Robart Hosiyan</a>
                </h5>
                <span class="designation">Business Advisor</span>
              </div>
            </div>
          </div>
          <div class="col-md-4 col-sm-6">
            <div class="team-item">
              <div class="thumb">
                <img src="assets/images/team/expert-06.png" alt="" />
                <ul
                  class="social-icons justify-content-center style-two bg-overlay-two d-flex flex-wrap"
                >
                  <li>
                    <a href="#"><i class="fab fa-twitter"></i></a>
                  </li>
                  <li>
                    <a href="#"><i class="fab fa-instagram"></i></a>
                  </li>
                  <li>
                    <a href="#"><i class="fab fa-facebook-f"></i></a>
                  </li>
                </ul>
              </div>
              <div class="content text-center">
                <h5 class="name">
                  <a href="team-details.html">Jamil Sheck</a>
                </h5>
                <span class="designation">Business Consultant</span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section> -->
    <!-- ==================== Team End Here ==================== -->

    <!-- ==================== Services Start Here ==================== -->
    @include('front.partials.service')
    <!-- ==================== Services End Here ==================== -->

    <!-- ==================== CEO Testimonial Start Here ==================== -->
    @include('front.partials.ceo')
    <!-- ==================== CEO Testimonial End Here ==================== -->

    <!-- ==================== Popular Work Start Here ==================== -->
   @include('front.partials.popular')
    <!-- ==================== Popular Work End Here ==================== -->

    <!-- ==================== Consulting Form Start Here ==================== -->
   @include('front.partials.consult')
    <!-- ==================== Consulting Form End Here ==================== -->

    <!-- ==================== Blog Start Here ==================== -->
   @include('front.partials.blog')
    <!-- ==================== Blog End Here ==================== -->

    <!-- ==================== CTA Start Here ==================== -->
   @include('front.partials.cta')
    <!-- ==================== CTA End Here ==================== -->

    <!-- ==================== Footer End Here ==================== -->
   @include('front.partials.footer')

    <div class="two"></div>
    <!-- ==================== Footer End Here ==================== -->

    <!-- ==================== Scroll to Top End Here ==================== -->
    <a class="scroll-top"><i class="fas fa-angle-double-up"></i></a>
    <!-- ==================== Scroll to Top End Here ==================== -->

  @include('front.partials.scripts')
  </body>
</html>
