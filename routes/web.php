<?php

use App\Http\Controllers\Backend\Admin\AdminLoginController;
use App\Http\Controllers\Backend\Admin\AdminRegisterController;
use App\Http\Controllers\Backend\Admin\ProfileController;
use App\Http\Controllers\Backend\Blog\BlogController;
use App\Http\Controllers\Backend\Blog\BlogDetailController;
use App\Http\Controllers\Backend\Configuration\ConfigController;
use App\Http\Controllers\Backend\CountUp\CountUpController;
use App\Http\Controllers\Backend\Faq\FaqController;
use App\Http\Controllers\Backend\MainService\MainServiceController;
use App\Http\Controllers\Backend\MainService\ServiceCatController;
use App\Http\Controllers\Backend\Offer\OfferController;
use App\Http\Controllers\Backend\Popular\PopularWorkController;
use App\Http\Controllers\Backend\ServiceCard\ServiceCardController;
use App\Http\Controllers\Backend\ServiceDetail\ServiceDetailController;
use App\Http\Controllers\Backend\Team\TeamController;
use App\Http\Controllers\Backend\Update\UpdateController;
use App\Http\Controllers\ContactMailController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\Frontend\Contact\ContactController;
use App\Http\Controllers\Frontend\Contact\ContactUsController;
use App\Http\Controllers\Frontend\FrontShowController;
use App\Http\Controllers\PageController;
use Faker\Guesser\Name;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

///////////////////front-end//////////////////////route//
Route::get('/', [PageController::class, 'indexpage'])->name('getindex');
Route::get('/index', [PageController::class, 'gethome'])->name('getHome');
Route::get('/about', [PageController::class, 'getabout'])->name('getAbout');
Route::get('/news', [PageController::class, 'getblog'])->name('getBlog');
Route::get('/news-detail/{slug}', [PageController::class, 'getblogdetail'])->name('getBlogDetail');
Route::get('/contact', [PageController::class, 'getcontact'])->name('getContact');
Route::get('/service', [PageController::class, 'getservice'])->name('getService');
Route::get('/service-detail/{slug}', [PageController::class, 'getservicedetail'])->name('getServiceDetail');
Route::get('/service/card/{slug1}', [PageController::class, 'getscdetail'])->name('getscDetail');
Route::get('/service/card-two/{slug2}', [PageController::class, 'getsctwo'])->name('getsctwo');
Route::get('/service/card-three/{slug3}', [PageController::class, 'getscthree'])->name('getscthree');


//contact route
Route::resource('/contactus', ContactController::class);
Route::post('/subscriber', [ContactUsController::class,'storeContact'])->name('store.contact');
Route::get('/subscriber', [ContactUsController::class, 'getContact'])->name('getSubs');
Route::delete('/del/subscriber/{id}', [ContactUsController::class, 'deleteSub'])->name('deleteSub');

//////////////Front-end routes END/////////////
//login
Route::match(['get','post'],'/login',[AdminLoginController::class, 'login'])->name('adminLogin');
//auth part
Route::group(['middleware'=>'auth'], function(){
//to dashboard
Route::get('/dashboard', [DashboardController::class, 'getIndex'])->name('index');
//get password
Route::get('/getpassowrd',[ProfileController::class,'password'])->name('getPassword');
//check password
Route::post('/check-password', [ProfileController::class, 'chkUserPassword'])->name('checkpassword');
//change password
Route::post('/change-password/{id}',[ProfileController::class,'updatePassword'])->name('changepassword');
//to get profile
Route::get('/getprofile', [ProfileController::class, 'profile'])->name('profile');
//update profile
Route::post('/update-profile/{id}',[ProfileController::class, 'updateProfile'])->name('updateprofile');

/////////Crud operation//////////
//ServiceCard
Route::resource('/service-card', ServiceCardController::class);
//Offer crud
Route::resource('/offers', OfferController::class);
//Popular work
Route::resource('/popularworks', PopularWorkController::class);
//Team member
Route::resource('/teams', TeamController::class);
//Blog crud
Route::resource('/blogs', BlogController::class);
//Main service
Route::resource('/mainservice', MainServiceController::class);
//Count ups
Route::resource('/countups', CountUpController::class);
//FAQ
Route::resource('/faqs', FaqController::class);
//Updates Routes
Route::get('update/content',[UpdateController::class, 'getUpdate'])->name('getupdate');

//page settings
Route::get('/settings', [ConfigController::class, 'getConfiguration'])->name('pageset');
Route::post('/settings', [ConfigController::class, 'postConfiguration'])->name('pageset.update');

});

//logout
Route::get('/logout', [AdminLoginController::class, 'logout'])->name('adminLogout');

// Route::post('/sendmail',[ContactMailController::class, 'sendEmail'])->name('sendemail');


