<?php

use App\Models\Configuration;
use App\Models\MainService;
use Illuminate\Support\Facades\Session;


// For Setting Pagee
function getConfiguration($key)
{
    $config = Configuration::where('configuration_key', '=', $key)->first();
    if ($config != null) {
        return $config->configuration_value;
    }
    return null;
}

function getServices()
{
    $getService = MainService::all();
    if($getService != null) {
        return $getService;
    }
    return null;
}

function footerService()
{
    $footerservice = MainService::limit(4)->get();
    if($footerservice != null){
        return $footerservice;
    }
    return null;
}
