<?php

namespace App\Models;

use App\Mail\ContactMail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Mail;

class Contact extends Model
{
    use HasFactory;
    protected $filable = ['useremail', 'message', 'subject','username'];

    public static function boot(){
        parent::boot();
        static::created(function ($item){
            $adminEmail = getConfiguration('primary_mail');
            Mail::to($adminEmail )->send(new ContactMail($item));
        });
    }
}
