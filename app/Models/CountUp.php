<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CountUp extends Model
{
    use HasFactory;
    protected $fillable = ['count_one', 'count_two', 'count_three', 'count_four'];
}
