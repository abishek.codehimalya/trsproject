<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MainService extends Model
{
    use HasFactory;
    protected $fillable = ['title', 'description', 'image','slug','detail_image'];
}
