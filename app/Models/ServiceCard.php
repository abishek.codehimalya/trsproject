<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ServiceCard extends Model
{
    use HasFactory;
    protected $fillable = ['title1','title2','title3','desc1','desc2','desc3', 'image1','image2','image3','slug1','slug2','slug3'];
}
