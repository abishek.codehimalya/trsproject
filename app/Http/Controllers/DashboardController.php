<?php

namespace App\Http\Controllers;

use App\Models\MainService;
use App\Models\Subscriber;
use App\Models\TeamMember;
use App\Models\User;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function getIndex()
    {
        $user = User::all();
        $team = TeamMember::all();
        $totalteam = count($team);
        $service = MainService::all();
        $totalservice = Count($service);
        $subscriber = Subscriber::all();
        $totalsubs = count($subscriber);
        return view('backend.layouts.dashcontent',compact('user','totalteam','totalservice','totalsubs'));
    }

    
}
