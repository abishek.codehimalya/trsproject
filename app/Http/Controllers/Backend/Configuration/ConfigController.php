<?php

namespace App\Http\Controllers\Backend\Configuration;

use App\Http\Controllers\Controller;
use App\Models\Configuration;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class ConfigController extends Controller
{
    public function getConfiguration(){
        return view('backend.config.settings');
    }

    public function postConfiguration(Request $request)
    {
        $inputs = $request->only(
            'logo_image', 'fabicon_image', 'ceo_image',
           'instagram','twitter','facebook','linkedin',
           'ceo_name','ceo_title','ceo_desc',
           'address','primary_phone','secondary_phone','primary_mail','secondary_mail','website',
        );
        //dd($inputs);
        foreach ($inputs as $inputKey =>$inputValue){
            if( $inputKey == 'logo_image'|| $inputKey == 'fabicon_image' || $inputKey == 'ceo_image'){
                $file = $inputValue;
                // dd($file);
                $this->deleteFile($inputKey);//delete old file
                $inputValue = $this->uploadFile($inputValue);// upload file
            }
            Configuration::updateOrCreate(['configuration_key' => $inputKey], ['configuration_value'=> $inputValue]);
        }
       // dd($inputValue);
        return redirect()->back()->with('success', 'Settings successfully updated');
    }

    protected function uploadFile($file)
    {
        $image_new_name = time() . $file->getClientOriginalName();
        $file->move('uploads/configurations', $image_new_name);
        return 'uploads/configurations/' .$image_new_name;
    }
    
    protected function deleteFile($inputKey)
    {
        $configuration = Configuration::where('configuration_key', '=', $inputKey)->first();
        //if configuration exists
        if (null !== $configuration && $configuration->exists())
        {
            if(file_exists('uploads/configurations/' . basename($configuration->configuration_value)))
            {
                file::delete('uploads/configurations/' . basename($configuration->configuration_value));
            }
        }
    }

}
