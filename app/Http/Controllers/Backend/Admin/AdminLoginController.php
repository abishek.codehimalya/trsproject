<?php

namespace App\Http\Controllers\Backend\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class AdminLoginController extends Controller
{
    public function getLogin()
    {
        return view('backend.admin.login');
    }

    public function getSignup()
    {
        return view('backend.admin.register');
    }

        //admin login
        public function login(Request $request){
            if($request->isMethod('post')){
                $data = $request->all();
            //   dd($data);
                $rules = [
                    'email' => 'required|email|max:255',
                    'password' => 'required'
                ];
                $customMessages = [
                    'email.required' => 'Email is required',
                    'email.email' => 'Enter a valid mail',
                    'email.max' => 'Not more than 255 characters',
                    'password.required' => 'Password is reqired',
                    'password.password' => 'Wrong passowrd'
                ];
                $this->validate($request, $rules, $customMessages);
              
                if(Auth::attempt(['email' => $data['email'], 'password' => $data['password']]))
                {
                    return redirect()->route('index');
                }
                else{
                    Session::flash('error', 'Invalid credentials');
                    return redirect()->route('adminLogin');
                }
            }
            //check if logged in
            if(Auth::check()){
                    return redirect()->route('index');
            }
            else{
                return view('backend.admin.login');        
            }
        }

        //logout
        
    //logout 
    public function logout(){
        Auth::logout();
        Session::flash('success_message', 'logout successful');
        return redirect()->route('adminLogin');
    }
}
