<?php

namespace App\Http\Controllers\Backend\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\File;

class ProfileController extends Controller
{
    public function profile(){

        $admin =  Auth::user();
        // dd($user);
        return view('backend.admin.profile', compact('admin'));
    }

    public function updateProfile(Request $request, $id){
        $data = $request->all();
        //validation
        $rules = [
            'email' => 'required|email|max:255',
            'name' => 'required|max:255'
        ];
        $customMessages = [
            'email.required' => 'Email is required',
            'email.email' => 'Enter a valid mail',
            'email.max' => 'Not more than 255 characters',
            'name.required' => 'Name is reqired',
            'name.max' => 'No more than 255 characters'
        ];
        $this->validate($request, $rules, $customMessages);//validation ends
        $profile=new User();
        $profile = $profile->find($id);
        $data = $request->all();
        if ($request->has('image')) {
            $file_name = "Image-" . date('ymdhis') . rand(0, 999) . "." . $request->image->getClientOriginalExtension();
            $path = public_path() . '/uploads/admin';
            if (!File::exists($path)) {
                File::makeDirectory($path, 0777, true, true);
            }
            $success = $request->image->move($path, $file_name);
            if ($success) {
               $data['image'] = $file_name;
            } else {
                $data['image'] = null;
            }
            $destination = 'uploads/admin/' . $profile->image;
            if (file::exists($destination)) {
                file::delete($destination);
            }
        }
        $profile->fill($data);
        $status = $profile->save();
        if ($status) {
            Session::flash('success', 'Profile updated Successfully');
            return redirect()->back();
        } 

    }

       //Change Admin Password
    public function password(){
        $admin = User::where('email', Auth::user()->email)->first();
        return view ('backend.admin.changepassword', compact('admin'));
    }

   //Check Admin Current Password
   public function chkUserPassword(Request $request){
    $data = $request->all();
    $current_password = $data['current_password'];
    $user_id = Auth::user()->id;
    $check_password = User::where('id', $user_id)->first();
    if (Hash::check($current_password, $check_password->password)){
        return "true"; die;
    } else {
        return "false"; die;
    }
}

// Update Admin password
 public function updatePassword(Request $request){
    $data = $request->all();
    $validateData = $request->validate([
        'current_password' => 'required|max:255',
        'password' => 'required',
        'password_confirmation' => 'required_with:password|same:password',
    ]);
    $admin = User::where('email', Auth::user()->email)->first();
    $current_password = $admin->password;
    if(Hash::check($data['current_password'], $current_password)){
        $admin->password = bcrypt($data['password']);
        $admin->save();
        Session::flash('success_message', 'Your Password Has Been Changed Successfully');
        return redirect()->back();
    } else {
        Session::flash('error_message', 'Your Current Password Does Not Match with our Database');
        return redirect()->back();
    }
}
}
