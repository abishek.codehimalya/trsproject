<?php

namespace App\Http\Controllers\Backend\Blog;

use App\Http\Controllers\Controller;
use App\Models\Blog;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;
// use RealRashid\SweetAlert\Facades\Alert;
use Alert;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $blogs = Blog::all();
        return view('backend.blogs.index', compact('blogs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.blogs.create')->with('success','Item created successfully.');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'name' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
            'author' => 'required',
            'date' => 'required',
        ]);
        $blog=new Blog();
        $data = $request->all();
        $blog->slug = Str::slug($request->title);
        if ($request->has('image')) {
            $file_name = "Image-" . date('ymdhis') . rand(0, 999) . "." . $request->image->getClientOriginalExtension();
            $path = public_path() . '/blogimage';
            if (!File::exists($path)) {
                File::makeDirectory($path, 0777, true, true);
            }
            $success = $request->image->move($path, $file_name);
            if ($success) {
               $data['image'] = $file_name;
            } else {
                $data['image'] = null;
            }

        }
        $blog->fill($data);
        $status = $blog->save();
        if ($status) {
            // session()->flash('success','Item created successfully.');
            return redirect()->route('blogs.index')->with('success','Blog added successfully');
        }
        // alert()->success('Success','Blog Added Successfully');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $blog = Blog::find($id);
        return view('backend.blogs.view', compact('blog'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $blog = Blog::find($id);
        return view('backend.blogs.edit', compact('blog'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'title' => 'required',
            'name' => 'required',
            'author' => 'required',
            'date' => 'required',
        ]);

        $blog=new Blog();
        $blog = $blog->find($id);
        $data = $request->all();
        $blog->slug = Str::slug($request->title);
        if ($request->has('image')) {
            $file_name = "Image-" . date('ymdhis') . rand(0, 999) . "." . $request->image->getClientOriginalExtension();
            $path = public_path() . '/blogimage';
            if (!File::exists($path)) {
                File::makeDirectory($path, 0777, true, true);
            }
            $destination = $path.'/'.$blog->image;
            if (file::exists($destination)) {
                file::delete($destination);
            }
            $success = $request->image->move($path, $file_name);
            if ($success) {
               $data['image'] = $file_name;
            } else {
                $data['image'] = null;
            }
            $destination = 'blogimage/' . $blog->image;
            if (file::exists($destination)) {
                file::delete($destination);
            }
        }
        $blog->fill($data);
        $status = $blog->save();
        if ($status) {
            Session::flash('success', 'Blog Updated Successfully');
            return redirect()->route('blogs.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $blog = new Blog();
        $blog = $blog->find($id);
        $destination = 'blogimage/' . $blog->image;
        if (file::exists($destination)) {
                file::delete($destination);
        }
        if ($blog) {
            $blog->delete();
        }
        return redirect()->route('blogs.index')->with('success', 'Blog deleted successfully!');

    }
}
