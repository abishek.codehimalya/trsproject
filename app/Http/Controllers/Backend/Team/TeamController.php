<?php

namespace App\Http\Controllers\Backend\Team;

use App\Http\Controllers\Controller;
use App\Models\TeamMember;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;

class TeamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $teams = TeamMember::all();
        return view('backend.team.index', compact('teams'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.team.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'name' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
        ]);
        $team=new TeamMember();
        $data = $request->all();
        $team->slug = Str::slug($request->name);
        if ($request->has('image')) {
            $file_name = "Image-" . date('ymdhis') . rand(0, 999) . "." . $request->image->getClientOriginalExtension();
            $path = public_path() . '/teamimage';
            if (!File::exists($path)) {
                File::makeDirectory($path, 0777, true, true);
            }
            $success = $request->image->move($path, $file_name);
            if ($success) {
               $data['image'] = $file_name;
            } else {
                $data['image'] = null;
            }
        }
        $team->fill($data);
        $status = $team->save();
        if ($status) {
            Session::flash('success', 'Team updated Successfully');
            return redirect()->route('teams.index');;
        } 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $team = TeamMember::find($id);
        return view('backend.team.view', compact('team'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $team = TeamMember::find($id);
        return view('backend.team.edit', compact('team'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'title' => 'required',
            'name' => 'required',
        ]);
        $team=new TeamMember();
        $team = $team->find($id);
        $data = $request->all();
        $team->slug = Str::slug($request->name);
        if ($request->has('image')) {
            $file_name = "Image-" . date('ymdhis') . rand(0, 999) . "." . $request->image->getClientOriginalExtension();
            $path = public_path() . '/teamimage';
            if (!File::exists($path)) {
                File::makeDirectory($path, 0777, true, true);
            }
            $success = $request->image->move($path, $file_name);
            if ($success) {
               $data['image'] = $file_name;
            } else {
                $data['image'] = null;
            }
            $destination = 'teamimage/' . $team->image;
            if (file::exists($destination)) {
                file::delete($destination);
            }
        }
        $team->fill($data);
        $status = $team->save();
        if ($status) {
            Session::flash('success', 'Team updated Successfully');
            return redirect()->route('teams.index');;
        } 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $team = new TeamMember();
        $team = $team->find($id);
        $destination = 'teamimage/' . $team->image;
        if (file::exists($destination)) {
                file::delete($destination);
        }
        if ($team) {
            $team->delete();
        }
        return redirect()->route('teams.index')->with('success', 'Team deleted successfully!');
        
    }
}
