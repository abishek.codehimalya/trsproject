<?php

namespace App\Http\Controllers\Backend\CountUp;

use App\Http\Controllers\Controller;
use App\Models\CountUp;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use PHPUnit\Framework\Constraint\Count;

class CountUpController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $id = 1;
        $countup = CountUp::where('id',$id)->first();
        return view('backend.countup.edit', compact('countup'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.countup.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'count_one' => 'required',
            'count_two' => 'required',
            'count_three' => 'required',
            'count_four' => 'required',

        ]);
        $countup = new CountUp();
        $data = $request->all();
        $countup->count_one = $data['count_one'];
        $countup->count_two = $data['count_two'];
        $countup->count_three = $data['count_three'];
        $countup->count_four = $data['count_four'];
        $countup->save();
        Session::flash('success','Data added Successfully');
        return redirect()->route('countups.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $countup = CountUp::find($id);
        return view('backend.countup.edit', compact('countup'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'count_one' => 'required',
            'count_two' => 'required',
            'count_three' => 'required',
            'count_four' => 'required',

        ]);
        $countup = new CountUp();
        $countup = $countup->find($id);
        $data = $request->all();
        $countup->count_one = $data['count_one'];
        $countup->count_two = $data['count_two'];
        $countup->count_three = $data['count_three'];
        $countup->count_four = $data['count_four'];
        $countup->update();
        Session::flash('success','CountUp updated Successfully');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $countup = CountUp::find($id);
        $countup->delete();
        Session::flash('success', 'Countup deleted Successfully');
        return redirect()->back();
    }
}
