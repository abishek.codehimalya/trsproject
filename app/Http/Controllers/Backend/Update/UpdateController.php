<?php

namespace App\Http\Controllers\Backend\Update;

use App\Http\Controllers\Controller;
use App\Models\CountUp;
use App\Models\Offer;
use App\Models\ServiceCard;
use Illuminate\Http\Request;

class UpdateController extends Controller
{
    public function getUpdate()
    {
        $service = [1,2,3];
        $about = 1;
        $count = 1;
        $countup = CountUp::where('id',$count)->first();
        $offer = Offer::where('id',$about)->first();
        $service = ServiceCard::where('id',$service)->first();
        return view('backend.updatetabs.update', compact('countup','offer','service'));
    }
}
