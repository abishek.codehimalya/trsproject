<?php

namespace App\Http\Controllers\Backend\ServiceCard;

use App\Http\Controllers\Controller;
use App\Models\ServiceCard;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\File;

class ServiceCardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $servicecards = ServiceCard::all();
        return view('backend.servicecard.index', compact('servicecards'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.servicecard.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $this->validate($request, [
            'title1' => 'required',
            'title2' => 'required',
            'title3' => 'required',
            'desc1' => 'required',
            'desc2' => 'required',
            'desc3' => 'required',
            'image1' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
            'image2' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
            'image3' => 'required|image|mimes:jpeg,png,jpg,gif,svg',

        ]);
        $service=new ServiceCard();
        $data = $request->all();
        $service->slug1 = Str::slug($request->title1);
        $service->slug2 = Str::slug($request->title2);
        $service->slug3 = Str::slug($request->title3);
        if ($request->has('image1')) {
            $file_name = "Image-" . date('ymdhis') . rand(0, 999) . "." . $request->image1->getClientOriginalExtension();
            $path = public_path() . '/servicecardimage';
            if (!File::exists($path)) {
                File::makeDirectory($path, 0777, true, true);
            }
            $success = $request->image1->move($path, $file_name);
            if ($success) {
               $data['image1'] = $file_name;
            } else {
                $data['image1'] = null;
            }
        }
        if ($request->has('image2')) {
            $file_name = "Image-" . date('ymdhis') . rand(0, 999) . "." . $request->image2->getClientOriginalExtension();
            $path = public_path() . '/servicecardimage';
            if (!File::exists($path)) {
                File::makeDirectory($path, 0777, true, true);
            }
            $success = $request->image2->move($path, $file_name);
            if ($success) {
               $data['image2'] = $file_name;
            } else {
                $data['image2'] = null;
            }
        }
        if ($request->has('image3')) {
            $file_name = "Image-" . date('ymdhis') . rand(0, 999) . "." . $request->image3->getClientOriginalExtension();
            $path = public_path() . '/servicecardimage';
            if (!File::exists($path)) {
                File::makeDirectory($path, 0777, true, true);
            }
            $success = $request->image3->move($path, $file_name);
            if ($success) {
               $data['image3'] = $file_name;
            } else {
                $data['image3'] = null;
            }
        }
        $service->fill($data);
        $status = $service->save();
        if ($status) {
            Session::flash('success', 'ServiceCard added Successfully');
            return redirect()->route('service-card.index');;
        } 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $service = ServiceCard::find($id);
        return view('backend.servicecard.view', compact('service'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $service = ServiceCard::findorFail($id);
        return view('backend.servicecard.edit', compact('service'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title1' => 'required',
            'title2' => 'required',
            'title3' => 'required',
            'desc1' => 'required',
            'desc2' => 'required',
            'desc3' => 'required',

        ]);
        $service=new ServiceCard();
        $service= $service->find($id);
        $data = $request->all();
        $service->slug1 = Str::slug($request->title1);
        $service->slug2 = Str::slug($request->title2);
        $service->slug3 = Str::slug($request->title3);
        if ($request->has('image1')) {
            $file_name = "Image-" . date('ymdhis') . rand(0, 999) . "." . $request->image1->getClientOriginalExtension();
            $path = public_path() . '/servicecardimage';
            if (!File::exists($path)) {
                File::makeDirectory($path, 0777, true, true);
            }
            $success = $request->image1->move($path, $file_name);
            if ($success) {
               $data['image1'] = $file_name;
            } else {
                $data['image1'] = null;
            }
            $destination = 'servicecardimage/' . $service->image1;
            if (file::exists($destination)) {
                file::delete($destination);
            }
        }
        if ($request->has('image2')) {
            $file_name = "Image-" . date('ymdhis') . rand(0, 999) . "." . $request->image2->getClientOriginalExtension();
            $path = public_path() . '/servicecardimage';
            if (!File::exists($path)) {
                File::makeDirectory($path, 0777, true, true);
            }
            $success = $request->image2->move($path, $file_name);
            if ($success) {
               $data['image2'] = $file_name;
            } else {
                $data['image2'] = null;
            }
            $destination = 'servicecardimage/' . $service->image2;
            if (file::exists($destination)) {
                file::delete($destination);
            }
        }
        if ($request->has('image3')) {
            $file_name = "Image-" . date('ymdhis') . rand(0, 999) . "." . $request->image3->getClientOriginalExtension();
            $path = public_path() . '/servicecardimage';
            if (!File::exists($path)) {
                File::makeDirectory($path, 0777, true, true);
            }
            $success = $request->image3->move($path, $file_name);
            if ($success) {
               $data['image3'] = $file_name;
            } else {
                $data['image3'] = null;
            }
            $destination = 'servicecardimage/' . $service->image3;
            if (file::exists($destination)) {
                file::delete($destination);
            }
        }
        $service->fill($data);
        $status = $service->save();
        if ($status) {
            Session::flash('success', 'ServiceCard updated Successfully');
            return redirect()->back();
        } 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $service = new ServiceCard();
        $service = $service->find($id);
        $destination1 = 'servicecardimage/' . $service->image1;
        if (file::exists($destination1)) {
                file::delete($destination1);
        }
        $destination2 = 'servicecardimage/' . $service->image2;
        if (file::exists($destination2)) {
                file::delete($destination2);
        }
        $destination3 = 'servicecardimage/' . $service->image3;
        if (file::exists($destination3)) {
                file::delete($destination3);
        }
        if ($service) {
            $service->delete();
        }
        return redirect()->route('service-card.index')->with('success', 'Servicecard deleted successfully!');
        


    }
}
