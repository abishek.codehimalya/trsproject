<?php

namespace App\Http\Controllers\Backend\Offer;

use App\Http\Controllers\Controller;
use App\Models\Offer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;

class OfferController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $id = 1;
        $offer = Offer::where('id',$id)->first();
        return view('backend.offers.edit', compact('offer'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.offers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'description' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
        ]);
        $offer=new Offer();
        $data = $request->all();
        if ($request->has('image')) {
            $file_name = "Image-" . date('ymdhis') . rand(0, 999) . "." . $request->image->getClientOriginalExtension();
            $path = public_path() . '/offerimage';
            if (!File::exists($path)) {
                File::makeDirectory($path, 0777, true, true);
            }
            $success = $request->image->move($path, $file_name);
            if ($success) {
               $data['image'] = $file_name;
            } else {
                $data['image'] = null;
            }
        }
        $offer->fill($data);
        $status = $offer->save();
        if ($status) {
            Session::flash('success', 'Data added Successfully');
            return redirect()->route('offers.index');;
        } 

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $offer = Offer::find($id);
        return view('backend.offers.view', compact('offer'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $offer = Offer::findorFail($id);
        return view('backend.offers.edit', compact('offer'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'title' => 'required',
            'description' => 'required',
        ]);
        $offer=new Offer();
        $offer = $offer->find($id);
        $data = $request->all();
        //  dd($data);
        if ($request->has('image')) {
            $file_name = "Image-" . date('ymdhis') . rand(0, 999) . "." . $request->image->getClientOriginalExtension();
            $path = public_path() . '/offerimage';
            if (!File::exists($path)) {
                File::makeDirectory($path, 0777, true, true);
            }
            $success = $request->image->move($path, $file_name);
            // dd($success);
            if ($success) {
               $data['image'] = $file_name;
            } else {
                $data['image'] = null;
            }
            $destination = 'offerimage/' . $offer->image;
            if (file::exists($destination)) {
                file::delete($destination);
            }
        }
        $offer->fill($data);
        $status = $offer->save();
        // dd($status);
        if ($status) {
            Session::flash('success', 'About updated Successfully');
            return redirect()->back();
        } 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $offer = new Offer();
        $offer = $offer->find($id);
        $destination = 'offerimage/' . $offer->image;
        if (file::exists($destination)) {
                file::delete($destination);
        }
        if ($offer) {
            $offer->delete();
        }
        return redirect()->route('offers.index')->with('success', 'About deleted successfully!');
        

    }
}
