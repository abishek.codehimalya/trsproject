<?php

namespace App\Http\Controllers\Backend\MainService;

use App\Http\Controllers\Controller;
use App\Models\MainService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;

class MainServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $mainservices = MainService::all();
        return view('backend.mainservice.index', compact('mainservices'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.mainservice.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'description' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
            'detail_image' => 'required|image|mimes:jpeg,png,jpg,gif,svg',

        ]);
        $mainservice=new MainService();
        $data = $request->all();
        $mainservice->slug = Str::slug($request->title);
        if ($request->has('image')) {
            $file_name = "Image-" . date('ymdhis') . rand(0, 999) . "." . $request->image->getClientOriginalExtension();
            $path = public_path() . '/mainserviceimage';
            if (!File::exists($path)) {
                File::makeDirectory($path, 0777, true, true);
            }
            $success = $request->image->move($path, $file_name);
            if ($success) {
               $data['image'] = $file_name;
            } else {
                $data['image'] = null;
            }
        }
        if ($request->has('detail_image')) {
            $file_name = "Image-" . date('ymdhis') . rand(0, 999) . "." . $request->detail_image->getClientOriginalExtension();
            $path = public_path() . '/mainserviceimage';
            if (!File::exists($path)) {
                File::makeDirectory($path, 0777, true, true);
            }
            $success = $request->detail_image->move($path, $file_name);
            if ($success) {
               $data['detail_image'] = $file_name;
            } else {
                $data['detail_image'] = null;
            }
        }
        $mainservice->fill($data);
        $status = $mainservice->save();
        if ($status) {
            Session::flash('success', 'Service added Successfully');
            return redirect()->route('mainservice.index');;
        } 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $mainservice = MainService::find($id);
        return view('backend.mainservice.view', compact('mainservice'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $mainservice = MainService::find($id);
        return view('backend.mainservice.edit', compact('mainservice'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'title' => 'required',
            'description' => 'required',
        ]);
        $mainservice=new MainService();
        $mainservice=$mainservice->find($id);        
        $data = $request->all();
        $mainservice->slug = Str::slug($request->title);
        if ($request->has('image')) {
            $file_name = "Image-" . date('ymdhis') . rand(0, 999) . "." . $request->image->getClientOriginalExtension();
            $path = public_path() . '/mainserviceimage';
            if (!File::exists($path)) {
                File::makeDirectory($path, 0777, true, true);
            }
            $success = $request->image->move($path, $file_name);
            if ($success) {
               $data['image'] = $file_name;
            } else {
                $data['image'] = null;
            }
            $destination = 'mainserviceimage/' . $mainservice->image;
            if (file::exists($destination)) {
                file::delete($destination);
            }
        }
        if ($request->has('detail_image')) {
            $file_name = "Image-" . date('ymdhis') . rand(0, 999) . "." . $request->detail_image->getClientOriginalExtension();
            $path = public_path() . '/mainserviceimage';
            if (!File::exists($path)) {
                File::makeDirectory($path, 0777, true, true);
            }
            $success = $request->detail_image->move($path, $file_name);
            if ($success) {
               $data['detail_image'] = $file_name;
            } else {
                $data['detail_image'] = null;
            }
            $destination1 = 'mainserviceimage/' . $mainservice->detail_image;
            if (file::exists($destination1)) {
                file::delete($destination1);
            }
        }
        $mainservice->fill($data);
        $status = $mainservice->save();
        if ($status) {
            Session::flash('success', 'Service updated Successfully');
            return redirect()->route('mainservice.index');;
        } 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $mainservice = new MainService();
        $mainservice = $mainservice->find($id);
        $destination1 = 'mainserviceimage/' . $mainservice->image;
        if (file::exists($destination1)) {
                file::delete($destination1);
        }
        $destination2 = 'mainserviceimage/' . $mainservice->detail_image;

        if (file::exists($destination2)) {
            file::delete($destination2);
    }
        if ($mainservice) {
            $mainservice->delete();
        }
        return redirect()->route('mainservice.index')->with('success', 'Service deleted successfully!');
        
    }
}
