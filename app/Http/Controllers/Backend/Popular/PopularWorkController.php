<?php

namespace App\Http\Controllers\Backend\Popular;

use App\Http\Controllers\Controller;
use App\Models\PopularWork;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;

class PopularWorkController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $populars = PopularWork::all();
        return view('backend.popular.index',compact('populars'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.popular.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'image_title' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
        ]);
        $popular=new PopularWork();
        $data = $request->all();
        $popular->slug = Str::slug($request->image_title);
        if ($request->has('image')) {
            $file_name = "Image-" . date('ymdhis') . rand(0, 999) . "." . $request->image->getClientOriginalExtension();
            $path = public_path() . '/popularimage';
            if (!File::exists($path)) {
                File::makeDirectory($path, 0777, true, true);
            }
            $success = $request->image->move($path, $file_name);
            if ($success) {
               $data['image'] = $file_name;
            } else {
                $data['image'] = null;
            }
            
        }
        $popular->fill($data);
        $status = $popular->save();
        if ($status) {
            Session::flash('success', 'Works added Successfully');
            return redirect()->route('popularworks.index');;
        } 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $popular = PopularWork::find($id);
        return view('backend.popular.view', compact('popular'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $popular = PopularWork::find($id);
        return view('backend.popular.edit', compact('popular'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'image_title' => 'required',
        ]);
        $popular=new PopularWork();
        $popular = $popular->find($id);
        $data = $request->all();
        $popular->slug = Str::slug($request->image_title);
        if ($request->has('image')) {
            $file_name = "Image-" . date('ymdhis') . rand(0, 999) . "." . $request->image->getClientOriginalExtension();
            $path = public_path() . '/popularimage';
            if (!File::exists($path)) {
                File::makeDirectory($path, 0777, true, true);
            }
            $success = $request->image->move($path, $file_name);
            if ($success) {
               $data['image'] = $file_name;
            } else {
                $data['image'] = null;
            }
            $destination = 'popularimage/' . $popular->image;
            if (file::exists($destination)) {
                file::delete($destination);
            }
        }
        $popular->fill($data);
        $status = $popular->save();
        if ($status) {
            Session::flash('success', 'Works updated Successfully');
            return redirect()->route('popularworks.index');;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $popular = new PopularWork();
        $popular = $popular->find($id);
        $destination = 'popularimage/' . $popular->image;
        if (file::exists($destination)) {
                file::delete($destination);
        }
        if ($popular) {
            $popular->delete();
        }
        return redirect()->route('popularworks.index')->with('success', 'Works deleted successfully!');
        
    }
}
