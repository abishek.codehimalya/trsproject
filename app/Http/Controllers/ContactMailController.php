<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail\ContactMail;
use Illuminate\Support\Facades\Mail;

class ContactMailController extends Controller
{
    public function sendEmail(Request $request)
    {
        $details = [
            'useremail' => $request->useremail,
            'subject' => $request->subject,
            'message' => $request->message,
        ];

        Mail::to(getConfiguration(('primary_mail')))->send(new ContactMail($details));
        return back()->with('success','Mail successfully sent');
    }
}
