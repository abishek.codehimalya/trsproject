<?php

namespace App\Http\Controllers\Frontend\Contact;

use App\Http\Controllers\Controller;
use App\Models\Contact;
use App\Models\Subscriber;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class ContactUsController extends Controller
{
    public function getContact()
    {
        $subscribers = Subscriber::all();
        return view('backend.subscriber.index', compact('subscribers'));
    }
    public function storeContact(Request $request)
    {
        $request->validate([
            'useremail' => 'required|email|unique:subscribers,useremail', 
        ]);

        $contactmail = new Subscriber();
        $data = $request->all();
        $contactmail->useremail = $data['useremail'];
        $status = $contactmail->save();
        return redirect()->back()->with('success', 'Subscribed');
    }
    public function deleteSub($id)
    {
        $subscriber = Subscriber::find($id);
        $subscriber->delete();
        Session::flash('success', 'Subscriber Deleted');
        return redirect()->back();

    }


    // public function storeRequest(Request $request)
    // {
    //     $request->validate([
    //         'username' => 'required',
    //     ]);
    //     $conrequest = new Contact();
    //     $data = $request->all();
    //     $conrequest->username = $data['username'];
    //     $conrequest->useremail = $data['useremail'];
    //     $conrequest->message = $data['message'];
    //     $conrequest->save();
    //     Session::flash('success', 'Mail Sent');
    //     return redirect()->back();
    // }
}
