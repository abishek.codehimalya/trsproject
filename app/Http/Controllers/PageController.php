<?php

namespace App\Http\Controllers;

use App\Models\Blog;
use App\Models\BlogDetail;
use App\Models\CountUp;
use App\Models\Faq;
use App\Models\MainService;
use App\Models\Offer;
use App\Models\PopularWork;
use App\Models\ServiceCard;
use App\Models\ServiceCat;
use App\Models\ServiceDetail;
use App\Models\TeamMember;
use Illuminate\Http\Request;

class PageController extends Controller
{
    public function indexpage()
    {
        $features = ServiceCard::limit(3)->get();
        $offers = Offer::limit(1)->get();
        $countups = CountUp::limit(1)->get();
        $populars = PopularWork::limit(6)->get();
        $frontblog = Blog::limit(3)->get();
        $faqs = Faq::limit(3)->get();
        return view('front.index',compact('features','offers','countups','populars','frontblog','faqs'));
    }
    public function gethome()
    {
        $features = ServiceCard::limit(3)->get();
        $offers = Offer::limit(1)->get();
        $countups = CountUp::limit(1)->get();
        $populars = PopularWork::limit(6)->get();
        $frontblog = Blog::limit(3)->get();
        $faqs = Faq::limit(3)->get();
        return view('front.index',compact('features','offers','countups','populars','frontblog','faqs'));
    }
    public function getabout()
    {
        $offers = Offer::limit(1)->get();
        $faqs = Faq::limit(3)->get();
        $teams = TeamMember::get();
        return view('front.pages.about', compact('offers','faqs','teams'));
    }

    public function getblog()
    {
        $frontblog = Blog::get();
        return view('front.pages.blog', compact('frontblog'));
    }

    public function getblogdetail($slug)
    {
        $frontblog = Blog::inRandomOrder()->limit(3)->get();
        $getblogdetail = Blog::where('slug',$slug)->get();
        return view('front.pages.blogdetail', compact('frontblog','getblogdetail'));
    }

    public function getcontact()
    {
        return view('front.pages.contact');
    }

    public function getservice()
    {
        $getallservice = MainService::limit(9)->get();
        $faqs = Faq::limit(3)->get();
        return view('front.pages.service', compact('getallservice','faqs'));
    }
    
    public function getservicedetail($slug)
    {
        $getslug = MainService::where('slug',$slug)->get();
        // $getdetail = MainService::limit(1)->get();
        return view('front.pages.servicedetail', compact('getslug'));
    }

    public function getscdetail($slug)
    {
        $getcard = ServiceCard::where('slug1',$slug)->get();
        return view('front.pages.servicecarddetail', compact('getcard'));
    }

    public function getsctwo($slug)
    {
        $getcardtwo = ServiceCard::where('slug2',$slug)->get();
        return view('front.pages.servicecardtwo', compact('getcardtwo'));
    }

    public function getscthree($slug)
    {
        $getcardthree = ServiceCard::where('slug3',$slug)->get();
        return view('front.pages.servicecardthree', compact('getcardthree'));
    }
}
